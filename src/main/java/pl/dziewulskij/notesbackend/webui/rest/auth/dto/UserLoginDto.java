package pl.dziewulskij.notesbackend.webui.rest.auth.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Getter
@AllArgsConstructor
@ToString(exclude = {"password"})
public class UserLoginDto {

    @NotBlank
    private final String username;

    @NotBlank
    private final String password;

}
