package pl.dziewulskij.notesbackend.webui.rest.notebook;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.dziewulskij.notesbackend.domain.notebook.service.NoteService;
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.BaseNoteDto;
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.NoteCreateEditDto;
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.NoteDto;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/notes")
public class NoteResource {

    private final NoteService noteService;

    public NoteResource(final NoteService noteService) {
        this.noteService = noteService;
    }

    @PostMapping("/notebooks/{notebookId}")
    public ResponseEntity<NoteDto> createNote(@PathVariable Long notebookId,
                                              @Valid @RequestBody NoteCreateEditDto noteCreateEditDto) {
        log.info("createNote for notebookId: {}, and object: {}", notebookId, noteCreateEditDto);
        return ResponseEntity.ok(noteService.createNote(notebookId, noteCreateEditDto));
    }

    @GetMapping("/{id}")
    public ResponseEntity<NoteDto> getNoteById(@PathVariable Long id) {
        log.info("getNoteById: {}", id);
        return ResponseEntity.ok(noteService.getNoteById(id));
    }

    @GetMapping("/notebooks/{notebookId}")
    public ResponseEntity<List<BaseNoteDto>> getAllNoteByNotebookId(@PathVariable Long notebookId) {
        log.info("getAllNoteByNotebookId: {}", notebookId);
        return ResponseEntity.ok(noteService.getAllNoteByNotebookId(notebookId));
    }

    @PutMapping("/{id}")
    public ResponseEntity<NoteDto> editNoteById(@PathVariable Long id,
                                                @Valid @RequestBody NoteCreateEditDto noteCreateEditDto) {
        log.info("editNoteById for notebookId: {}, and object: {}", id, noteCreateEditDto);
        return ResponseEntity.ok(noteService.editNoteById(id, noteCreateEditDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteNoteById(@PathVariable Long id) {
        log.info("deleteNoteById: {}", id);
        noteService.deleteNoteById(id);
        return ResponseEntity.ok().build();
    }
}
