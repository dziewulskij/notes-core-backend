package pl.dziewulskij.notesbackend.webui.rest.notebook;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryDto;
import pl.dziewulskij.notesbackend.domain.notebook.service.NotebookService;
import pl.dziewulskij.notesbackend.webui.rest.common.PageResponse;
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.BaseNotebookDto;
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.NotebookDto;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/notebooks")
public class NotebookResource {

    private final NotebookService notebookService;

    public NotebookResource(final NotebookService notebookService) {
        this.notebookService = notebookService;
    }

    @PostMapping
    public ResponseEntity<NotebookDto> createNotebook(@Valid @RequestBody BaseNotebookDto baseNotebookDto) {
        log.info("createNotebook: {}", baseNotebookDto);
        return ResponseEntity.ok(notebookService.createNotebook(baseNotebookDto));
    }

    @GetMapping("/{id}")
    public ResponseEntity<NotebookDto> getNotebookById(@PathVariable Long id) {
        log.info("getNotebookById: {}", id);
        return ResponseEntity.ok(notebookService.getNotebookById(id));
    }

    @GetMapping("/dictionaries")
    public ResponseEntity<List<DictionaryDto>> getNotebookDictionaries() {
        log.info("getNotebookDictionaries");
        return ResponseEntity.ok(notebookService.getNotebookDictionaries());
    }

    @GetMapping
    public ResponseEntity<PageResponse> getAllNotebookPageable(Pageable pageable) {
        log.info("getAllNotebookPageable");
        var notebookPageable = notebookService.getAllNotebookPageable(pageable);
        return ResponseEntity.ok(PageResponse.build(notebookPageable));
    }

    @PutMapping("/{id}")
    public ResponseEntity<NotebookDto> editNotebookById(@PathVariable Long id,
                                                        @Valid @RequestBody BaseNotebookDto baseNotebookDto) {
        log.info("editNotebookById: {}, object: {}", id, baseNotebookDto);
        return ResponseEntity.ok(notebookService.editNotebookById(id, baseNotebookDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteNotebookById(@PathVariable Long id) {
        log.info("editNotebookById: {}", id);
        notebookService.deleteNotebookById(id);
        return ResponseEntity.ok().build();
    }

}
