package pl.dziewulskij.notesbackend.webui.rest.auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.dziewulskij.notesbackend.domain.auth.AuthenticationService;
import pl.dziewulskij.notesbackend.webui.rest.auth.dto.JwtDto;
import pl.dziewulskij.notesbackend.webui.rest.auth.dto.UserLoginDto;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/api/authentication")
public class AuthenticationResource {

    private final AuthenticationService authenticationService;

    public AuthenticationResource(final AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping
    public ResponseEntity<JwtDto> login(@Valid @RequestBody UserLoginDto userLoginDto) {
        log.info("Login user with username: {}", userLoginDto.getUsername());
        var jwtDto = new JwtDto(authenticationService.login(userLoginDto));
        return ResponseEntity.ok(jwtDto);
    }

}
