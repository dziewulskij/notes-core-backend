package pl.dziewulskij.notesbackend.webui.rest.user.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.dziewulskij.notesbackend.domain.user.repository.UserRepository;
import pl.dziewulskij.notesbackend.webui.rest.user.dto.UserCreateDto;

import java.util.Optional;

import static pl.dziewulskij.notesbackend.webui.rest.user.validator.UserValidatorConstant.*;

@Component
public class UserCreateValidator implements Validator {

    private final UserRepository userRepository;

    public UserCreateValidator(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return UserCreateDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object object, Errors errors) {
        UserCreateDto userCreateDto = (UserCreateDto) object;
        checkUniqueUsername(userCreateDto.getUsername(), errors);
        checkUniqueEmail(userCreateDto.getEmail(), errors);
        checkConfirmationPasswords(userCreateDto.getPassword(), userCreateDto.getConfirmationPassword(), errors);
    }

    private void checkUniqueUsername(String username, Errors errors) {
        Optional.of(username)
                .filter(userRepository::existsByUsername)
                .ifPresent(val -> errors.rejectValue(USERNAME_FIELD, USERNAME_UNIQUE_ERROR));
    }

    private void checkUniqueEmail(String email, Errors errors) {
        Optional.of(email)
                .filter(userRepository::existsByEmail)
                .ifPresent(val -> errors.rejectValue(EMAIL_FIELD, EMAIL_UNIQUE_ERROR));
    }

    private void checkConfirmationPasswords(String password, String confirmationPassword, Errors errors) {
        if (!password.equals(confirmationPassword)) {
            errors.rejectValue(CONFIRMATION_PASSWORD_FIELD, CONFIRMATION_PASSWORD_EQUAL_ERROR);
        }
    }
}
