package pl.dziewulskij.notesbackend.webui.rest.user.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static pl.dziewulskij.notesbackend.webui.rest.user.validator.UserValidatorConstant.PASSWORD_PATTERN;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@SuperBuilder(toBuilder = true)
@ToString(exclude = {"password", "confirmationPassword"})
public class UserCreateDto extends UserSimpleDto {

    @NotBlank
    @Size(max = 30)
    private String username;

    @NotBlank
    @Size(min = 8, max = 20)
    @Pattern(regexp = PASSWORD_PATTERN)
    private String password;

    @NotBlank
    @Size(min = 8, max = 20)
    private String confirmationPassword;

}
