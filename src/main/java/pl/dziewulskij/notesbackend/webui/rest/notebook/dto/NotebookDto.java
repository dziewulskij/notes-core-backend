package pl.dziewulskij.notesbackend.webui.rest.notebook.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryDto;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@SuperBuilder(toBuilder = true)
public class NotebookDto {

    private Long id;
    private String title;
    private DictionaryDto owner;
    private List<DictionaryDto> notes;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

}
