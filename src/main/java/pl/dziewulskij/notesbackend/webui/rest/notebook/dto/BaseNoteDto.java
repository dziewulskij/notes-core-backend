package pl.dziewulskij.notesbackend.webui.rest.notebook.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@SuperBuilder(toBuilder = true)
public class BaseNoteDto {

    private Long id;
    private String title;
    private String subtitle;
    private LocalDateTime createdAt;

}
