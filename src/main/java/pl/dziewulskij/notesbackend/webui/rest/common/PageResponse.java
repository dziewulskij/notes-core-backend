package pl.dziewulskij.notesbackend.webui.rest.common;

import lombok.Builder;
import lombok.Getter;
import org.springframework.data.domain.Page;

import java.util.Collection;

@Getter
@Builder
public class PageResponse {

    private final int totalPages;
    private final int pageNumber;
    private final long totalElements;
    private final Collection<?> content;

    public static PageResponse build(Page<?> page) {
        return PageResponse.builder()
                .totalPages(page.getTotalPages())
                .pageNumber(page.getPageable().getPageNumber())
                .totalElements(page.getTotalElements())
                .content(page.getContent())
                .build();
    }

}
