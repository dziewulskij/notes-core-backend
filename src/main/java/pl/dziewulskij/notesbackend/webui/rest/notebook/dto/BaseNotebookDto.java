package pl.dziewulskij.notesbackend.webui.rest.notebook.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@SuperBuilder(toBuilder = true)
public class BaseNotebookDto {

    @NotBlank
    private String title;

}
