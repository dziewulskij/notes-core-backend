package pl.dziewulskij.notesbackend.webui.rest.notebook.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@Builder(toBuilder = true)
public class NoteCreateEditDto {

    @NotBlank
    private String title;
    private String subtitle;

    @Size(max = 5000)
    private String content;
}