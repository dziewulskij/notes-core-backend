package pl.dziewulskij.notesbackend.webui.rest.user.validator;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.dziewulskij.notesbackend.core.jwt.JwtUtil;
import pl.dziewulskij.notesbackend.domain.user.model.User;
import pl.dziewulskij.notesbackend.domain.user.repository.UserRepository;
import pl.dziewulskij.notesbackend.webui.rest.user.dto.ChangeOwnPasswordDto;

import static pl.dziewulskij.notesbackend.webui.rest.user.validator.UserValidatorConstant.*;

@Component
public class ChangeOwnPasswordValidator implements Validator {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public ChangeOwnPasswordValidator(final UserRepository userRepository,
                                      final PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return ChangeOwnPasswordDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object object, Errors errors) {
        ChangeOwnPasswordDto changeOwnPasswordDto = (ChangeOwnPasswordDto) object;
        checkOldPassword(changeOwnPasswordDto, errors);
        checkConfirmationPasswords(changeOwnPasswordDto.getPassword(), changeOwnPasswordDto.getConfirmationPassword(), errors);
    }

    private void checkOldPassword(ChangeOwnPasswordDto changeOwnPasswordDto, Errors errors) {
        var loggedUserId = JwtUtil.getJwtUser().getId();
        userRepository.findById(loggedUserId)
                .map(User::getPassword)
                .filter(password -> !passwordEncoder.matches(changeOwnPasswordDto.getOldPassword(), password))
                .ifPresent(password -> errors.rejectValue(OLD_PASSWORD_FIELD, OLD_PASSWORD_EQUAL_ERROR));
    }

    private void checkConfirmationPasswords(String password, String confirmationPassword, Errors errors) {
        if (!password.equals(confirmationPassword)) {
            errors.rejectValue(CONFIRMATION_PASSWORD_FIELD, CONFIRMATION_PASSWORD_EQUAL_ERROR);
        }
    }
}
