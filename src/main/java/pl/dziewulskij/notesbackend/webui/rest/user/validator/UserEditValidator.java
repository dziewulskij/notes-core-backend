package pl.dziewulskij.notesbackend.webui.rest.user.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.dziewulskij.notesbackend.domain.user.repository.UserRepository;
import pl.dziewulskij.notesbackend.webui.rest.user.dto.UserEditDto;
import pl.dziewulskij.notesbackend.webui.rest.user.dto.UserSimpleDto;

import java.util.Optional;

import static io.vavr.API.*;
import static io.vavr.Predicates.instanceOf;
import static pl.dziewulskij.notesbackend.webui.rest.user.validator.UserValidatorConstant.EMAIL_FIELD;
import static pl.dziewulskij.notesbackend.webui.rest.user.validator.UserValidatorConstant.EMAIL_UNIQUE_ERROR;

@Component
public class UserEditValidator implements Validator {

    private final UserRepository userRepository;

    public UserEditValidator(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return UserEditDto.class.isAssignableFrom(clazz) || UserSimpleDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object object, Errors errors) {
        Match(object).of(
                Case($(instanceOf(UserEditDto.class)), () -> run(() -> userEditDtoValidate(object, errors))),
                Case($(instanceOf(UserSimpleDto.class)), () -> run(() -> userSimpleDtoValidate(object, errors)))
        );
    }

    private void userEditDtoValidate(Object object, Errors errors) {
        UserEditDto userEditDto = (UserEditDto) object;
        checkUniqueEmail(userEditDto.getId(), userEditDto.getEmail(), errors);
    }

    private void userSimpleDtoValidate(Object object, Errors errors) {
        UserSimpleDto userSimpleDto = (UserSimpleDto) object;
        checkUniqueEmail(userSimpleDto.getId(), userSimpleDto.getEmail(), errors);
    }

    private void checkUniqueEmail(Long id, String email, Errors errors) {
        Optional.of(email)
                .filter(mail -> userRepository.existsByEmailExcludedId(mail, id))
                .ifPresent(val -> errors.rejectValue(EMAIL_FIELD, EMAIL_UNIQUE_ERROR));
    }

}
