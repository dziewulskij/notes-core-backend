package pl.dziewulskij.notesbackend.webui.rest.user.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static pl.dziewulskij.notesbackend.webui.rest.user.validator.UserValidatorConstant.EMAIL_PATTERN;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@SuperBuilder(toBuilder = true)
public class UserSimpleDto {

    private Long id;

    @NotBlank
    @Size(max = 50)
    private String name;

    @NotBlank
    @Size(max = 50)
    private String surname;

    @NotBlank
    @Size(max = 100)
    @Pattern(regexp = EMAIL_PATTERN)
    private String email;
}
