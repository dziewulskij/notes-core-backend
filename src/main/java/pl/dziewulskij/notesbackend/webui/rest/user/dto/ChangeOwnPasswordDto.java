package pl.dziewulskij.notesbackend.webui.rest.user.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static pl.dziewulskij.notesbackend.webui.rest.user.validator.UserValidatorConstant.PASSWORD_PATTERN;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@ToString(exclude = {"oldPassword", "password", "confirmationPassword"})
public class ChangeOwnPasswordDto {

    @NotBlank
    private String oldPassword;

    @NotBlank
    @Size(min = 8, max = 20)
    @Pattern(regexp = PASSWORD_PATTERN)
    private String password;

    @NotBlank
    @Size(min = 8, max = 20)
    private String confirmationPassword;

}
