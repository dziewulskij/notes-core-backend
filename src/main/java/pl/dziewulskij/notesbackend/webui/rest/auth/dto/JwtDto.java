package pl.dziewulskij.notesbackend.webui.rest.auth.dto;

import lombok.Getter;

@Getter
public class JwtDto {

    private final String jwt;
    private final String tokenType = "Bearer";

    public JwtDto(String jwt) {
        this.jwt = jwt;
    }
}

