package pl.dziewulskij.notesbackend.webui.rest.authority;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.dziewulskij.notesbackend.domain.authority.service.AuthorityService;
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryDto;

import java.util.Set;

import static pl.dziewulskij.notesbackend.core.common.enumerated.AuthorityType.AuthorityCode.ROLE_ADMIN;

@Slf4j
@RestController
@RequestMapping("/api/authorities")
public class AuthorityResource {

    private final AuthorityService authorityService;

    public AuthorityResource(final AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    @Secured(ROLE_ADMIN)
    @GetMapping
    public ResponseEntity<Set<DictionaryDto>> getAllAuthoritiesDictionary() {
        log.info("getAllAuthoritiesDictionary");
        return ResponseEntity.ok(authorityService.getAllAuthoritiesDictionary());
    }

}
