package pl.dziewulskij.notesbackend.webui.rest.user.param;

import lombok.Getter;
import lombok.ToString;

import java.beans.ConstructorProperties;
import java.util.Set;

@Getter
@ToString
public class UserFilterRequestParam {

    private final String email;
    private final String username;
    private final Boolean enable;
    private final Set<Long> authorityIds;

    @ConstructorProperties({"email", "username", "enable", "authority-ids"})
    public UserFilterRequestParam(final String email,
                                  final String username,
                                  final Boolean enable,
                                  final Set<Long> authorityIds) {
        this.email = email;
        this.username = username;
        this.enable = enable;
        this.authorityIds = authorityIds;
    }

}