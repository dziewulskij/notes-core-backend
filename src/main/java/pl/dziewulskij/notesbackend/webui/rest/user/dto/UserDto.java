package pl.dziewulskij.notesbackend.webui.rest.user.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryDto;

import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@SuperBuilder(toBuilder = true)
public class UserDto extends UserSimpleDto {

    private boolean enable;
    private String username;
    private Set<DictionaryDto> authorities;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
