package pl.dziewulskij.notesbackend.webui.rest.user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import pl.dziewulskij.notesbackend.domain.user.service.UserService;
import pl.dziewulskij.notesbackend.webui.rest.common.PageResponse;
import pl.dziewulskij.notesbackend.webui.rest.user.dto.*;
import pl.dziewulskij.notesbackend.webui.rest.user.param.UserFilterRequestParam;
import pl.dziewulskij.notesbackend.webui.rest.user.validator.ChangeOwnPasswordValidator;
import pl.dziewulskij.notesbackend.webui.rest.user.validator.UserCreateValidator;
import pl.dziewulskij.notesbackend.webui.rest.user.validator.UserEditValidator;

import javax.validation.Valid;

import static pl.dziewulskij.notesbackend.core.common.enumerated.AuthorityType.AuthorityCode.ROLE_ADMIN;

@Slf4j
@RestController
@RequestMapping("/api/users")
public class UserResource {

    private final UserService userService;
    private final UserCreateValidator userCreateValidator;
    private final UserEditValidator userEditValidator;
    private final ChangeOwnPasswordValidator changeOwnPasswordValidator;

    public UserResource(final UserService userService,
                        final UserCreateValidator userCreateValidator,
                        final UserEditValidator userEditValidator,
                        final ChangeOwnPasswordValidator changeOwnPasswordValidator) {
        this.userService = userService;
        this.userCreateValidator = userCreateValidator;
        this.userEditValidator = userEditValidator;
        this.changeOwnPasswordValidator = changeOwnPasswordValidator;
    }

    @InitBinder(value = "userCreateDto")
    public void initUserCreateValidator(WebDataBinder webDataBinder) {
        webDataBinder.setValidator(userCreateValidator);
    }

    @InitBinder(value = {"userEditDto", "userSimpleDto"})
    public void initUserEditValidator(WebDataBinder webDataBinder) {
        webDataBinder.setValidator(userEditValidator);
    }

    @InitBinder(value = "changeOwnPasswordDto")
    public void initChangeOwnPasswordValidator(WebDataBinder webDataBinder) {
        webDataBinder.setValidator(changeOwnPasswordValidator);
    }

    @PostMapping
    public ResponseEntity<UserDto> createUser(@Valid @RequestBody UserCreateDto userCreateDto) {
        log.info("Create user: {}", userCreateDto);
        return ResponseEntity.ok(userService.createUser(userCreateDto));
    }

    @PreAuthorize("@authorityHolder.hasAuthorityToUserObjectById(#id)")
    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable Long id) {
        log.info("getUserById: {}", id);
        return ResponseEntity.ok(userService.getUserById(id));
    }

    @Secured(ROLE_ADMIN)
    @GetMapping
    public ResponseEntity<PageResponse> getAllPageable(UserFilterRequestParam userFilterRequestParam,
                                                       Pageable pageable) {
        log.info("getAllPageable and filter: {}", userFilterRequestParam);
        var usersPage = userService.getAllPageable(userFilterRequestParam, pageable);
        return ResponseEntity.ok(PageResponse.build(usersPage));
    }

    @Secured(ROLE_ADMIN)
    @PutMapping("/by-admin")
    public ResponseEntity<UserDto> editUserByAdmin(@Valid @RequestBody UserEditDto userEditDto) {
        log.info("editUserByAdmin with object: {}", userEditDto);
        return ResponseEntity.ok(userService.editUserByAdmin(userEditDto));
    }

    @PreAuthorize("@authorityHolder.hasAuthorityToUserObjectById(#userSimpleDto.id)")
    @PutMapping("/own")
    public ResponseEntity<UserDto> editUserByClient(@Valid @RequestBody UserSimpleDto userSimpleDto) {
        log.info("editUserByClient with object: {}", userSimpleDto);
        return ResponseEntity.ok(userService.editUserByClient(userSimpleDto));
    }

    @Secured(ROLE_ADMIN)
    @PutMapping("/{id}")
    public void enableOrDisableUserById(@PathVariable Long id, @RequestParam boolean enable) {
        log.info("enableOrDisableUserById: {}, enable: {}", id, enable);
        userService.enableOrDisableUserById(id, enable);
    }

    @PutMapping("/own/passwords")
    public void changeOwnPassword(@Valid @RequestBody ChangeOwnPasswordDto changeOwnPasswordDto) {
        log.info("changeOwnPassword");
        userService.changeOwnPassword(changeOwnPasswordDto);
    }

}
