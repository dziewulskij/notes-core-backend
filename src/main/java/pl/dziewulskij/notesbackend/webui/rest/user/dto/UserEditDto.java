package pl.dziewulskij.notesbackend.webui.rest.user.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@SuperBuilder(toBuilder = true)
public class UserEditDto extends UserSimpleDto {

    private boolean enable;
    private Set<Long> authorityIds;
}
