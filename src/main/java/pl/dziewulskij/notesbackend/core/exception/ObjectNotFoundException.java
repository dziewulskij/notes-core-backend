package pl.dziewulskij.notesbackend.core.exception;

public class ObjectNotFoundException extends RuntimeException {

    private static final String DEFAULT_MESSAGE = "Object not found";
    private static final String DEFAULT_MESSAGE_WITH_ID = "Object not found with id ";

    public ObjectNotFoundException() {
        super(DEFAULT_MESSAGE);
    }

    public ObjectNotFoundException(Long id) {
        super(DEFAULT_MESSAGE_WITH_ID + id);
    }

}
