package pl.dziewulskij.notesbackend.core.exception;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Builder
public class ErrorResponse {

    private final LocalDateTime timestamp;
    private final Set<ErrorDetails> errorDetails;

    public static ErrorResponse build(Set<ErrorDetails> errorDetails) {
        return ErrorResponse.builder()
                .timestamp(LocalDateTime.now())
                .errorDetails(errorDetails)
                .build();
    }
}
