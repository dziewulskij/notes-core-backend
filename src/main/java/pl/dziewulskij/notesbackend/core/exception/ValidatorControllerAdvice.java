package pl.dziewulskij.notesbackend.core.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;

@ControllerAdvice
public class ValidatorControllerAdvice extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        var errorDetails = exception.getBindingResult().getFieldErrors().stream()
                .map(this::buildErrorDetails)
                .collect(Collectors.toSet());
        var errorResponse = ErrorResponse.build(errorDetails);
        return ResponseEntity.badRequest().body(errorResponse);
    }

    private ErrorDetails buildErrorDetails(FieldError fieldError) {
        return ErrorDetails.builder()
                .field(fieldError.getField())
                .code(fieldError.getCode())
                .build();
    }

}
