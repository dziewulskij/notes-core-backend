package pl.dziewulskij.notesbackend.core.mapper.note;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import pl.dziewulskij.notesbackend.domain.notebook.model.Note;
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.BaseNoteDto;
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.NoteCreateEditDto;
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.NoteDto;

@Mapper
public interface NoteMapper {

    Note mapToNote(NoteCreateEditDto noteCreateEditDto);

    Note mapToNote(NoteCreateEditDto noteCreateEditDto, @MappingTarget Note note);

    NoteDto mapToNoteDto(Note note);

    BaseNoteDto mapToBaseNoteDto(Note note);

}
