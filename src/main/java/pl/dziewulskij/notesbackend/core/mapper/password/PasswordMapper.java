package pl.dziewulskij.notesbackend.core.mapper.password;

import org.mapstruct.Named;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class PasswordMapper {

    private final PasswordEncoder passwordEncoder;

    public PasswordMapper(final PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Named("encodePassword")
    public String map(String password) {
        return passwordEncoder.encode(password);
    }

}
