package pl.dziewulskij.notesbackend.core.mapper.user;

import org.mapstruct.*;
import pl.dziewulskij.notesbackend.core.mapper.dictionary.DictionaryMapper;
import pl.dziewulskij.notesbackend.core.mapper.password.PasswordMapper;
import pl.dziewulskij.notesbackend.domain.authority.model.Authority;
import pl.dziewulskij.notesbackend.domain.user.model.User;
import pl.dziewulskij.notesbackend.webui.rest.user.dto.UserCreateDto;
import pl.dziewulskij.notesbackend.webui.rest.user.dto.UserDto;
import pl.dziewulskij.notesbackend.webui.rest.user.dto.UserEditDto;
import pl.dziewulskij.notesbackend.webui.rest.user.dto.UserSimpleDto;

import java.util.Set;
import java.util.stream.Collectors;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR,
        uses = {
                PasswordMapper.class,
                DictionaryMapper.class
        },
        imports = PasswordMapper.class)
public interface UserMapper {

    UserDto mapToUserDto(User user);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "enable", constant = "true")
    @Mapping(target = "password", qualifiedByName = "encodePassword")
    User mapToUser(UserCreateDto userCreateDto);

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "authorityIds", target = "authorities", qualifiedByName = "getAuthoritiesFromIds")
    User mapToEdit(UserEditDto userEditDto, @MappingTarget User user);

    @Mapping(target = "id", ignore = true)
    User mapToEdit(UserSimpleDto userSimpleDto, @MappingTarget User user);

    @Named("getAuthoritiesFromIds")
    default Set<Authority> getAuthoritiesFromIds(Set<Long> authorityIds) {
        return authorityIds.stream()
                .map(id -> Authority.builder().id(id).build())
                .collect(Collectors.toSet());
    }

}
