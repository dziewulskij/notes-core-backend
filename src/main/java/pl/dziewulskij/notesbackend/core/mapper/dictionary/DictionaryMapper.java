package pl.dziewulskij.notesbackend.core.mapper.dictionary;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import pl.dziewulskij.notesbackend.domain.authority.model.Authority;
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryDto;
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryView;
import pl.dziewulskij.notesbackend.domain.notebook.model.Note;
import pl.dziewulskij.notesbackend.domain.user.model.User;

import java.util.List;
import java.util.Set;

@Mapper
public interface DictionaryMapper {

    DictionaryDto mapToDictionaryDto(DictionaryView dictionaryView);

    @Mapping(source = "authorityType", target = "name")
    DictionaryDto mapToDictionaryDto(Authority authority);

    Set<DictionaryDto> mapToDictionaryDtos(Set<Authority> authorities);

    @Named("mapUserToDictionaryDto")
    @Mapping(target = "name", expression = "java(user.getName() + \" \" + user.getSurname())")
    DictionaryDto mapToDictionaryDto(User user);

    @Named("mapNoteToDictionaryDto")
    @Mapping(source = "title", target = "name")
    DictionaryDto mapToDictionaryDto(Note note);

    @Named("mapNoteToDictionaryDtos")
    @IterableMapping(qualifiedByName = "mapNoteToDictionaryDto")
    List<DictionaryDto> mapToDictionaryDto(Set<Note> note);

}