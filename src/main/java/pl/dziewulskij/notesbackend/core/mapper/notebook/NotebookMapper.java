package pl.dziewulskij.notesbackend.core.mapper.notebook;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import pl.dziewulskij.notesbackend.core.mapper.dictionary.DictionaryMapper;
import pl.dziewulskij.notesbackend.domain.notebook.model.Notebook;
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.BaseNotebookDto;
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.NotebookDto;

@Mapper(uses = DictionaryMapper.class, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface NotebookMapper {

    Notebook mapToNotebook(BaseNotebookDto baseNotebookDto);

    @Mapping(source = "owner", target = "owner", qualifiedByName = "mapUserToDictionaryDto")
    @Mapping(source = "notes", target = "notes", qualifiedByName = "mapNoteToDictionaryDtos")
    NotebookDto mapToNotebookDto(Notebook notebook);

    Notebook mapToNotebook(BaseNotebookDto baseNotebookDto, @MappingTarget Notebook notebook);

}
