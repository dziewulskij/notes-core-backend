package pl.dziewulskij.notesbackend.core.common.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CustomClaim {

    USER_ID("userId"),
    USERNAME("username"),
    AUTHORITIES("authorities");

    private final String name;

}
