package pl.dziewulskij.notesbackend.core.common.enumerated;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

public enum AuthorityType implements GrantedAuthority {

    ROLE_ADMIN(AuthorityCode.ROLE_ADMIN),
    ROLE_USER(AuthorityCode.ROLE_USER);

    @Getter
    private final String name;

    AuthorityType(String name) {
        this.name = name;
    }

    @Override
    public String getAuthority() {
        return name;
    }

    public static class AuthorityCode {
        public static final String ROLE_USER = "ROLE_USER";
        public static final String ROLE_ADMIN = "ROLE_ADMIN";
    }
}
