package pl.dziewulskij.notesbackend.core.jwt;

import io.jsonwebtoken.Jwts;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import pl.dziewulskij.notesbackend.core.common.enumerated.CustomClaim;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static pl.dziewulskij.notesbackend.core.common.enumerated.CustomClaim.USER_ID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JwtUtil {

    private static final String BEARER_NAME = "Bearer ";

    public static Optional<Long> getUserId(String token) {
        return Optional.of(getTokenWithoutSecret(token))
                .flatMap(tokenWithoutSecret -> getClaims(tokenWithoutSecret, USER_ID))
                .map(Long::parseLong);
    }

    public static Optional<String> resolveToken(HttpServletRequest httpServletRequest) {
        return Optional.ofNullable(httpServletRequest.getHeader(AUTHORIZATION))
                .filter(token -> StringUtils.hasText(token) && token.startsWith(BEARER_NAME))
                .map(token -> token.substring(BEARER_NAME.length()));
    }

    public static JwtUser getJwtUser() {
        return (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    private static String getTokenWithoutSecret(String token) {
        int signatureIndex = token.lastIndexOf('.');
        return token.substring(0, signatureIndex + 1);
    }

    private static Optional<String> getClaims(String token, CustomClaim customClaim) {
        var claim = Jwts.parser()
                .parseClaimsJwt(token)
                .getBody()
                .get(customClaim.getName());
        return Optional.ofNullable(claim)
                .map(Object::toString);
    }

}
