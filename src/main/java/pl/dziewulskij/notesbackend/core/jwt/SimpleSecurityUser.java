package pl.dziewulskij.notesbackend.core.jwt;

import lombok.Builder;
import lombok.Getter;
import pl.dziewulskij.notesbackend.domain.user.model.User;

@Getter
@Builder
public final class SimpleSecurityUser {

    private final Long id;
    private final String username;

    public static SimpleSecurityUser build(User user) {
        return SimpleSecurityUser.builder()
                .id(user.getId())
                .username(user.getUsername())
                .build();
    }

}
