package pl.dziewulskij.notesbackend.core.jwt;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.dziewulskij.notesbackend.domain.user.service.UserDaoService;

@Slf4j
@Service
public class SecurityUserDetailsService implements UserDetailsService {

    private final UserDaoService userDaoService;

    public SecurityUserDetailsService(final UserDaoService userDaoService) {
        this.userDaoService = userDaoService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userDaoService.findUserPrincipalByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));
    }

}
