package pl.dziewulskij.notesbackend.core.jwt;

import lombok.Builder;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import pl.dziewulskij.notesbackend.domain.authority.AuthorityUtil;
import pl.dziewulskij.notesbackend.domain.user.model.User;

import java.util.Collection;

@Getter
@Builder
public final class JwtUser {

    private final Long id;
    private final String username;
    private final Collection<? extends GrantedAuthority> authorities;

    public static JwtUser build(User user) {
        var authorities = AuthorityUtil.getAuthorityNames(user);
        return JwtUser.builder()
                .id(user.getId())
                .username(user.getUsername())
                .authorities(authorities)
                .build();
    }

}
