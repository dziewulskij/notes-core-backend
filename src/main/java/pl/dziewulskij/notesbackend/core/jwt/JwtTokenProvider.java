package pl.dziewulskij.notesbackend.core.jwt;

import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import pl.dziewulskij.notesbackend.domain.authority.AuthorityUtil;

import java.util.Date;

import static pl.dziewulskij.notesbackend.core.common.enumerated.CustomClaim.*;

@Slf4j
@Component
public class JwtTokenProvider {

    @Value("${jwt.key.secret}")
    private String jwtSecret;

    @Value("${jwt.expiration-in-ms}")
    private int jwtExpirationInMs;

    @Value("${spring.application.name}")
    private String jwtSubject;

    public String generateToken(Authentication authentication) {
        var userPrincipal = (UserPrincipal) authentication.getPrincipal();
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);

        return Jwts.builder()
                .setSubject(jwtSubject)
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .claim(USER_ID.getName(), userPrincipal.getId())
                .claim(USERNAME.getName(), userPrincipal.getUsername())
                .claim(AUTHORITIES.getName(), AuthorityUtil.getAuthorityNames(userPrincipal))
                .compact();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
            return true;
        } catch (SignatureException ex) {
            log.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            log.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            log.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            log.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            log.error("JWT claims string is empty.");
        }
        return false;
    }

}
