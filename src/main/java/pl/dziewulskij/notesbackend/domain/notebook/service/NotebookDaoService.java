package pl.dziewulskij.notesbackend.domain.notebook.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import pl.dziewulskij.notesbackend.core.exception.ObjectNotFoundException;
import pl.dziewulskij.notesbackend.core.jwt.JwtUtil;
import pl.dziewulskij.notesbackend.core.mapper.dictionary.DictionaryMapper;
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryDto;
import pl.dziewulskij.notesbackend.domain.notebook.model.Notebook;
import pl.dziewulskij.notesbackend.domain.notebook.repository.NotebookRepository;
import pl.dziewulskij.notesbackend.domain.user.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
public class NotebookDaoService {

    private final NotebookRepository notebookRepository;
    private final UserRepository userRepository;
    private final DictionaryMapper dictionaryMapper;

    public NotebookDaoService(final NotebookRepository notebookRepository,
                              final UserRepository userRepository,
                              final DictionaryMapper dictionaryMapper) {
        this.notebookRepository = notebookRepository;
        this.userRepository = userRepository;
        this.dictionaryMapper = dictionaryMapper;
    }

    public Notebook create(Notebook notebook) {
        log.info("create: {}", notebook);
        var userId = JwtUtil.getJwtUser().getId();
        userRepository.findById(userId)
                .ifPresentOrElse(
                        user -> user.addNotebook(notebook),
                        this::thrownObjectNotFoundException);
        return notebookRepository.save(notebook);
    }

    public Optional<Notebook> findNotebookById(Long id) {
        log.info("findNotebookById: {}", id);
        return notebookRepository.findById(id);
    }

    public List<DictionaryDto> findNotebookDictionariesByUserId(Long userId) {
        log.info("findNotebookDictionariesByUserId: {}", userId);
        return notebookRepository.findNotebookDictionariesByUserId(userId).stream()
                .map(dictionaryMapper::mapToDictionaryDto)
                .collect(Collectors.toList());
    }

    public Page<Notebook> findAllNotebookPageable(Long userId, Pageable pageable) {
        log.info("findAllNotebookPageable");
        return notebookRepository.findAllPageable(userId, pageable);
    }

    public Notebook save(Notebook notebook) {
        log.info("save: {}", notebook);
        return notebookRepository.save(notebook);
    }

    public void deleteById(Long id) {
        log.info("deleteById: {}", id);
        notebookRepository.deleteById(id);
    }

    private void thrownObjectNotFoundException() {
        log.error("thrownObjectNotFoundException");
        throw new ObjectNotFoundException();
    }
}
