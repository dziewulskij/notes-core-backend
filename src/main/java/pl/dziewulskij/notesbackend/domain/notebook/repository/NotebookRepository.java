package pl.dziewulskij.notesbackend.domain.notebook.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryView;
import pl.dziewulskij.notesbackend.domain.notebook.model.Notebook;

import java.util.List;

public interface NotebookRepository extends JpaRepository<Notebook, Long> {

    @Query("select n.id as id, n.title as name from Notebook n where n.owner.id = ?1")
    List<DictionaryView> findNotebookDictionariesByUserId(Long userId);

    @Query("select n from Notebook n where n.owner.id = ?1")
    Page<Notebook> findAllPageable(Long userId, Pageable pageable);

}
