package pl.dziewulskij.notesbackend.domain.notebook.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import pl.dziewulskij.notesbackend.domain.audit.AbstractDateAudit;

import javax.persistence.*;

@Getter
@Setter
@Entity
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@Table(name = "notes")
public class Note extends AbstractDateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notes_generator_store")
    @GenericGenerator(
            name = "notes_generator_store",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "notes_generator_seq"),
                    @Parameter(name = "initial_value", value = "1"),
                    @Parameter(name = "increment_size", value = "1")
            }
    )
    private Long id;

    @Column(nullable = false, length = 30)
    private String title;

    @Column(length = 100)
    private String subtitle;

    @Column(length = 5000)
    private String content;

}
