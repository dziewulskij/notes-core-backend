package pl.dziewulskij.notesbackend.domain.notebook.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import pl.dziewulskij.notesbackend.core.exception.ObjectNotFoundException;
import pl.dziewulskij.notesbackend.core.jwt.JwtUtil;
import pl.dziewulskij.notesbackend.core.mapper.notebook.NotebookMapper;
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryDto;
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.BaseNotebookDto;
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.NotebookDto;

import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class NotebookService {

    private final NotebookDaoService notebookDaoService;
    private final NotebookMapper notebookMapper;

    public NotebookService(final NotebookDaoService notebookDaoService,
                           final NotebookMapper notebookMapper) {
        this.notebookDaoService = notebookDaoService;
        this.notebookMapper = notebookMapper;
    }

    public NotebookDto createNotebook(BaseNotebookDto baseNotebookDto) {
        log.info("createNotebook: {}", baseNotebookDto);
        return Optional.of(baseNotebookDto)
                .map(notebookMapper::mapToNotebook)
                .map(notebookDaoService::create)
                .map(notebookMapper::mapToNotebookDto)
                .orElseThrow(ObjectNotFoundException::new);
    }

    public NotebookDto getNotebookById(Long id) {
        log.info("getNotebookById: {}", id);
        return notebookDaoService.findNotebookById(id)
                .map(notebookMapper::mapToNotebookDto)
                .orElseThrow(ObjectNotFoundException::new);
    }

    public List<DictionaryDto> getNotebookDictionaries() {
        log.info("getNotebookDictionaries");
        var userId = JwtUtil.getJwtUser().getId();
        return notebookDaoService.findNotebookDictionariesByUserId(userId);
    }

    public Page<NotebookDto> getAllNotebookPageable(Pageable pageable) {
        log.info("getAllNotebookPageable");
        var userId = JwtUtil.getJwtUser().getId();
        return notebookDaoService.findAllNotebookPageable(userId, pageable)
                .map(notebookMapper::mapToNotebookDto);
    }

    public NotebookDto editNotebookById(Long id, BaseNotebookDto baseNotebookDto) {
        log.info("editNotebookById: {}, object: {}", id, baseNotebookDto);
        return notebookDaoService.findNotebookById(id)
                .map(notebook -> notebookMapper.mapToNotebook(baseNotebookDto, notebook))
                .map(notebookDaoService::save)
                .map(notebookMapper::mapToNotebookDto)
                .orElseThrow(ObjectNotFoundException::new);
    }

    public void deleteNotebookById(Long id) {
        log.info("deleteNotebookById: {}", id);
        notebookDaoService.deleteById(id);
    }
}
