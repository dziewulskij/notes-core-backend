package pl.dziewulskij.notesbackend.domain.notebook.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.dziewulskij.notesbackend.core.exception.ObjectNotFoundException;
import pl.dziewulskij.notesbackend.core.mapper.note.NoteMapper;
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.BaseNoteDto;
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.NoteCreateEditDto;
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.NoteDto;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
public class NoteService {

    private final NoteDaoService noteDaoService;
    private final NoteMapper noteMapper;

    public NoteService(final NoteDaoService noteDaoService,
                       final NoteMapper noteMapper) {
        this.noteDaoService = noteDaoService;
        this.noteMapper = noteMapper;
    }

    public NoteDto createNote(Long notebookId, NoteCreateEditDto noteCreateEditDto) {
        log.info("createNote for notebookId: {}, and object: {}", notebookId, noteCreateEditDto);
        return Optional.of(noteCreateEditDto)
                .map(noteMapper::mapToNote)
                .map(note -> noteDaoService.create(notebookId, note))
                .map(noteMapper::mapToNoteDto)
                .orElseThrow();
    }

    public NoteDto getNoteById(Long id) {
        log.info("getNoteById: {}", id);
        return noteDaoService.findById(id)
                .map(noteMapper::mapToNoteDto)
                .orElseThrow(ObjectNotFoundException::new);
    }

    public List<BaseNoteDto> getAllNoteByNotebookId(Long notebookId) {
        log.info("getAllNoteByNotebookId: {}", notebookId);
        return noteDaoService.findAllNoteByNotebookId(notebookId).stream()
                .map(noteMapper::mapToBaseNoteDto)
                .collect(Collectors.toList());
    }

    public NoteDto editNoteById(Long id, NoteCreateEditDto noteCreateEditDto) {
        log.info("editNoteById for notebookId: {}, and object: {}", id, noteCreateEditDto);
        return noteDaoService.findById(id)
                .map(note -> noteMapper.mapToNote(noteCreateEditDto, note))
                .map(noteDaoService::save)
                .map(noteMapper::mapToNoteDto)
                .orElseThrow(ObjectNotFoundException::new);
    }

    public void deleteNoteById(Long id) {
        log.info("deleteNoteById: {}", id);
        noteDaoService.deleteById(id);
    }
}
