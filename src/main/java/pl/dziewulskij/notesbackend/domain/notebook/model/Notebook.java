package pl.dziewulskij.notesbackend.domain.notebook.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import pl.dziewulskij.notesbackend.domain.audit.AbstractDateAudit;
import pl.dziewulskij.notesbackend.domain.user.model.User;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@Table(name = "notebooks")
public class Notebook extends AbstractDateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notebooks_generator_store")
    @GenericGenerator(
            name = "notebooks_generator_store",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "notebooks_generator_seq"),
                    @Parameter(name = "initial_value", value = "1"),
                    @Parameter(name = "increment_size", value = "1")
            }
    )
    private Long id;

    @Column(nullable = false, length = 30)
    private String title;

    @ManyToOne(fetch = FetchType.LAZY)
    private User owner;

    @Builder.Default
    @Fetch(FetchMode.JOIN)
    @OneToMany(cascade = {CascadeType.DETACH, CascadeType.REMOVE}, orphanRemoval = true)
    @JoinColumn(name = "notebook_id")
    private Set<Note> notes = new HashSet<>();

    public void addNote(Note note) {
        notes.add(note);
    }
}
