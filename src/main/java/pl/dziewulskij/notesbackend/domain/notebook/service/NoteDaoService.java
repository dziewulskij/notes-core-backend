package pl.dziewulskij.notesbackend.domain.notebook.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.dziewulskij.notesbackend.core.exception.ObjectNotFoundException;
import pl.dziewulskij.notesbackend.domain.notebook.model.Note;
import pl.dziewulskij.notesbackend.domain.notebook.repository.NoteRepository;
import pl.dziewulskij.notesbackend.domain.notebook.repository.NotebookRepository;

import java.util.List;
import java.util.Optional;

@Slf4j
@Component
class NoteDaoService {

    private final NotebookRepository notebookRepository;
    private final NoteRepository noteRepository;

    public NoteDaoService(final NotebookRepository notebookRepository,
                          final NoteRepository noteRepository) {
        this.notebookRepository = notebookRepository;
        this.noteRepository = noteRepository;
    }

    public Note create(Long notebookId, Note note) {
        log.info("create for notebookId: {}, and object: {}", notebookId, note);
        notebookRepository.findById(notebookId)
                .ifPresentOrElse(notebook -> notebook.addNote(note), this::thrownObjectNotFoundException);
        return noteRepository.save(note);
    }

    public Optional<Note> findById(Long id) {
        log.info("findNoteById: {}", id);
        return noteRepository.findById(id);
    }

    public List<Note> findAllNoteByNotebookId(Long notebookId) {
        log.info("findAllNoteByNotebookId: {}", notebookId);
        return noteRepository.findAllByNotebookId(notebookId);
    }

    public Note save(Note note) {
        log.info("save: {}", note);
        return noteRepository.save(note);
    }

    public void deleteById(Long id) {
        log.info("deleteById: {}", id);
        noteRepository.deleteById(id);
    }

    private void thrownObjectNotFoundException() {
        log.error("thrownObjectNotFoundException");
        throw new ObjectNotFoundException();
    }
}
