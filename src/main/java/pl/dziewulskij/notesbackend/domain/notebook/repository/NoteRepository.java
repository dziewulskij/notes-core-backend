package pl.dziewulskij.notesbackend.domain.notebook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.dziewulskij.notesbackend.domain.notebook.model.Note;

import java.util.List;

public interface NoteRepository extends JpaRepository<Note, Long> {

    @Query(value = "selóect * from notes where notebook_id = :notebookId ", nativeQuery = true)
    List<Note> findAllByNotebookId(Long notebookId);

}
