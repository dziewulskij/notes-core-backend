package pl.dziewulskij.notesbackend.domain.dictionary;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@Builder
@AllArgsConstructor
public final class DictionaryDto {

    private final Long id;
    private final String name;
}
