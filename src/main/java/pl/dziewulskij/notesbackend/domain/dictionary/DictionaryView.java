package pl.dziewulskij.notesbackend.domain.dictionary;

public interface DictionaryView {

    Long getId();

    String getName();
}
