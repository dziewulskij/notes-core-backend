package pl.dziewulskij.notesbackend.domain.authority;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import pl.dziewulskij.notesbackend.core.jwt.UserPrincipal;
import pl.dziewulskij.notesbackend.domain.authority.model.Authority;
import pl.dziewulskij.notesbackend.domain.user.model.User;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AuthorityUtil {

    public static Set<SimpleGrantedAuthority> getAuthorityNames(User user) {
        return user.getAuthorities().stream()
                .map(Authority::getAuthorityType)
                .map(Enum::name)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
    }

    public static Set<String> getAuthorityNames(UserPrincipal userPrincipal) {
        return userPrincipal.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet());
    }

    public static Set<String> getAuthorityNames(Collection<? extends GrantedAuthority> authorities) {
        return authorities.stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet());
    }

}
