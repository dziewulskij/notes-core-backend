package pl.dziewulskij.notesbackend.domain.authority.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.dziewulskij.notesbackend.core.common.enumerated.AuthorityType;
import pl.dziewulskij.notesbackend.domain.authority.model.Authority;
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryView;

import java.util.Optional;
import java.util.Set;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    Optional<Authority> findByAuthorityType(AuthorityType authorityType);

    @Query("select a.id as id, a.authorityType as name from Authority a")
    Set<DictionaryView> getAllAuthoritiesDictionaryView();

}
