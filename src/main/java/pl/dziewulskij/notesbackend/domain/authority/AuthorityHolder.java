package pl.dziewulskij.notesbackend.domain.authority;

import org.springframework.stereotype.Component;
import pl.dziewulskij.notesbackend.core.jwt.JwtUtil;

import static pl.dziewulskij.notesbackend.core.common.enumerated.AuthorityType.ROLE_ADMIN;
import static pl.dziewulskij.notesbackend.core.common.enumerated.AuthorityType.ROLE_USER;

@Component
public class AuthorityHolder {

    public boolean hasAuthorityToUserObjectById(Long id) {
        var jwtUser = JwtUtil.getJwtUser();
        var userAuthorityNames = AuthorityUtil.getAuthorityNames(jwtUser.getAuthorities());

        if (userAuthorityNames.contains(ROLE_ADMIN.getAuthority())) {
            return true;
        }
        if (userAuthorityNames.contains(ROLE_USER.getAuthority())) {
            return jwtUser.getId().equals(id);
        }
        return false;
    }

}
