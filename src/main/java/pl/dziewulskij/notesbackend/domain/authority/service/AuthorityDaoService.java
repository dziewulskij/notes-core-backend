package pl.dziewulskij.notesbackend.domain.authority.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.dziewulskij.notesbackend.domain.authority.repository.AuthorityRepository;
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryView;

import java.util.Set;

@Slf4j
@Service
public class AuthorityDaoService {

    private final AuthorityRepository authorityRepository;

    public AuthorityDaoService(final AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    public Set<DictionaryView> getAllAuthoritiesDictionary() {
        log.info("getAllAuthoritiesDictionary");
        return authorityRepository.getAllAuthoritiesDictionaryView();
    }
}
