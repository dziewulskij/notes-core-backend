package pl.dziewulskij.notesbackend.domain.authority.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.dziewulskij.notesbackend.core.mapper.dictionary.DictionaryMapper;
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryDto;

import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AuthorityService {

    private final AuthorityDaoService authorityDaoService;
    private final DictionaryMapper dictionaryMapper;

    public AuthorityService(final AuthorityDaoService authorityDaoService,
                            final DictionaryMapper dictionaryMapper) {
        this.authorityDaoService = authorityDaoService;
        this.dictionaryMapper = dictionaryMapper;
    }

    public Set<DictionaryDto> getAllAuthoritiesDictionary() {
        log.info("getAllAuthoritiesDictionary");
        return authorityDaoService.getAllAuthoritiesDictionary().stream()
                .map(dictionaryMapper::mapToDictionaryDto)
                .collect(Collectors.toSet());
    }
}
