package pl.dziewulskij.notesbackend.domain.auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.dziewulskij.notesbackend.core.jwt.JwtTokenProvider;
import pl.dziewulskij.notesbackend.webui.rest.auth.dto.UserLoginDto;

@Slf4j
@Service
public class AuthenticationService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;

    public AuthenticationService(final AuthenticationManager authenticationManager,
                                 final JwtTokenProvider jwtTokenProvider) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    public String login(UserLoginDto userLoginDto) {
        log.info("Login user with username: {}", userLoginDto.getUsername());
        var authentication = authenticate(userLoginDto);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return jwtTokenProvider.generateToken(authentication);
    }

    private Authentication authenticate(UserLoginDto userLoginDto) {
        return authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        userLoginDto.getUsername(),
                        userLoginDto.getPassword()
                )
        );
    }

}
