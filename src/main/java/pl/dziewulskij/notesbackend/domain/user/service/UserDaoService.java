package pl.dziewulskij.notesbackend.domain.user.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.dziewulskij.notesbackend.core.common.enumerated.AuthorityType;
import pl.dziewulskij.notesbackend.core.exception.ObjectNotFoundException;
import pl.dziewulskij.notesbackend.core.jwt.UserPrincipal;
import pl.dziewulskij.notesbackend.domain.authority.model.Authority;
import pl.dziewulskij.notesbackend.domain.authority.repository.AuthorityRepository;
import pl.dziewulskij.notesbackend.domain.user.model.User;
import pl.dziewulskij.notesbackend.domain.user.repository.UserRepository;
import pl.dziewulskij.notesbackend.webui.rest.user.param.UserFilterRequestParam;

import java.util.Optional;

@Slf4j
@Service
public class UserDaoService {

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;

    public UserDaoService(final UserRepository userRepository,
                          final AuthorityRepository authorityRepository) {
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
    }

    public Optional<User> findUserById(Long userId) {
        log.info("findUserById: {}", userId);
        return userRepository.findById(userId);
    }

    public Optional<UserPrincipal> findUserPrincipalByUsername(String username) {
        log.info("findUserPrincipalByUsername: {}", username);
        return userRepository.findByUsername(username)
                .map(UserPrincipal::build);
    }

    public Page<User> findAllPageable(UserFilterRequestParam userFilterRequestParam, Pageable pageable) {
        log.info("findAllPageable and filter: {}", userFilterRequestParam);
        return userRepository.findAllPageable(userFilterRequestParam, pageable);
    }

    public User createUser(User user) {
        log.info("createUser with username: {}", user.getUsername());
        var userAuthority = findAuthority(AuthorityType.ROLE_USER);
        user.getAuthorities().add(userAuthority);
        return userRepository.save(user);
    }

    public User editUser(User user) {
        log.info("editUser with id: {}", user.getId());
        return userRepository.save(user);
    }

    public void enableOrDisableUserById(Long userId, boolean enable) {
        log.info("enableOrDisableUserById: {}, enable: {}", userId, enable);
        findUserById(userId)
                .map(user -> prepareToEnableOrDisable(user, enable))
                .map(userRepository::save)
                .orElseThrow(() -> new ObjectNotFoundException(userId));
    }

    public void changeOwnPassword(Long userId, String newPassword) {
        log.info("changeOwnPassword");
        findUserById(userId)
                .map(user -> user.toBuilder().password(newPassword).build())
                .map(userRepository::save)
                .orElseThrow(() -> new ObjectNotFoundException(userId));
    }

    private Authority findAuthority(AuthorityType authorityType) {
        return authorityRepository.findByAuthorityType(authorityType)
                .orElseThrow();
    }

    private User prepareToEnableOrDisable(User user, boolean enable) {
        user.setEnable(enable);
        return user;
    }
}
