package pl.dziewulskij.notesbackend.domain.user.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import pl.dziewulskij.notesbackend.domain.audit.AbstractDateAudit;
import pl.dziewulskij.notesbackend.domain.authority.model.Authority;
import pl.dziewulskij.notesbackend.domain.notebook.model.Notebook;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@Table(name = "users")
public class User extends AbstractDateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_generator_store")
    @GenericGenerator(
            name = "users_generator_store",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "users_generator_seq"),
                    @Parameter(name = "initial_value", value = "10"),
                    @Parameter(name = "increment_size", value = "1")
            }
    )
    private Long id;

    @Column(nullable = false, length = 50)
    private String name;

    @Column(nullable = false, length = 50)
    private String surname;

    @Column(nullable = false, unique = true, length = 30)
    private String username;

    @Column(nullable = false, unique = true, length = 100)
    private String email;

    @Column(name = "password_hash", nullable = false)
    private String password;

    @Column(nullable = false)
    private boolean enable;

    @Builder.Default
    @Fetch(FetchMode.JOIN)
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_authorities",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "authority_id")
    )
    private Set<Authority> authorities = new HashSet<>();

    @Builder.Default
    @Fetch(FetchMode.JOIN)
    @OneToMany(mappedBy = "owner", cascade = {CascadeType.DETACH, CascadeType.REMOVE}, orphanRemoval = true)
    private Set<Notebook> notebooks = new HashSet<>();

    public void addNotebook(Notebook notebook) {
        notebooks.add(notebook);
        notebook.setOwner(this);
    }

}