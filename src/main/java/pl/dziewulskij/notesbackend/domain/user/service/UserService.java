package pl.dziewulskij.notesbackend.domain.user.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.dziewulskij.notesbackend.core.exception.ObjectNotFoundException;
import pl.dziewulskij.notesbackend.core.jwt.JwtUtil;
import pl.dziewulskij.notesbackend.core.mapper.password.PasswordMapper;
import pl.dziewulskij.notesbackend.core.mapper.user.UserMapper;
import pl.dziewulskij.notesbackend.webui.rest.user.dto.*;
import pl.dziewulskij.notesbackend.webui.rest.user.param.UserFilterRequestParam;

import java.util.Optional;

@Slf4j
@Service
public class UserService {

    private final UserMapper userMapper;
    private final PasswordMapper passwordMapper;
    private final UserDaoService userDaoService;

    public UserService(final UserMapper userMapper,
                       final PasswordMapper passwordMapper,
                       final UserDaoService userDaoService) {
        this.userMapper = userMapper;
        this.passwordMapper = passwordMapper;
        this.userDaoService = userDaoService;
    }

    public UserDto createUser(UserCreateDto userCreateDto) {
        log.info("createUser: {}", userCreateDto);
        return Optional.of(userCreateDto)
                .map(userMapper::mapToUser)
                .map(userDaoService::createUser)
                .map(userMapper::mapToUserDto)
                .orElseThrow();
    }

    public UserDto getUserById(Long id) {
        log.info("getUserById: {}", id);
        return userDaoService.findUserById(id)
                .map(userMapper::mapToUserDto)
                .orElseThrow(() -> new ObjectNotFoundException(id));
    }

    public Page<UserDto> getAllPageable(UserFilterRequestParam userFilterRequestParam, Pageable pageable) {
        log.info("getAllPageable and filter: {}", userFilterRequestParam);
        return userDaoService.findAllPageable(userFilterRequestParam, pageable)
                .map(userMapper::mapToUserDto);
    }

    public UserDto editUserByAdmin(UserEditDto userEditDto) {
        log.info("editUserByAdmin with object: {}", userEditDto);
        return userDaoService.findUserById(userEditDto.getId())
                .map(user -> userMapper.mapToEdit(userEditDto, user))
                .map(userDaoService::editUser)
                .map(userMapper::mapToUserDto)
                .orElseThrow(() -> new ObjectNotFoundException(userEditDto.getId()));
    }

    public UserDto editUserByClient(UserSimpleDto userSimpleDto) {
        log.info("editUserByClient with object: {}", userSimpleDto);
        return userDaoService.findUserById(userSimpleDto.getId())
                .map(user -> userMapper.mapToEdit(userSimpleDto, user))
                .map(userDaoService::editUser)
                .map(userMapper::mapToUserDto)
                .orElseThrow(() -> new ObjectNotFoundException(userSimpleDto.getId()));
    }

    public void enableOrDisableUserById(Long id, boolean enable) {
        log.info("enableOrDisableUserById: {}, enable: {}", id, enable);
        userDaoService.enableOrDisableUserById(id, enable);
    }

    public void changeOwnPassword(ChangeOwnPasswordDto changeOwnPasswordDto) {
        log.info("changeOwnPassword");
        var userId = JwtUtil.getJwtUser().getId();
        var newPassword = passwordMapper.map(changeOwnPasswordDto.getPassword());
        userDaoService.changeOwnPassword(userId, newPassword);
    }

}
