package pl.dziewulskij.notesbackend.domain.user.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.dziewulskij.notesbackend.domain.user.model.User;
import pl.dziewulskij.notesbackend.webui.rest.user.param.UserFilterRequestParam;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);

    @Query("select case when count(distinct u.id) > 0 then true else false end " +
            "from User u where u.email = ?1 and u.id <> ?2")
    boolean existsByEmailExcludedId(String email, Long id);

    @Query("select u from User u left join u.authorities a where " +
            "(:#{#userFilterRequestParam.email} is null or u.email like %?#{[0].email}%) " +
            "and (:#{#userFilterRequestParam.username} is null or u.username like %?#{[0].username}%) " +
            "and (:#{#userFilterRequestParam.enable} is null or u.enable = :#{#userFilterRequestParam.enable}) " +
            "and (coalesce(:#{#userFilterRequestParam.authorityIds}, null) is null or a.id in (:#{#userFilterRequestParam.authorityIds}))")
    Page<User> findAllPageable(UserFilterRequestParam userFilterRequestParam, Pageable pageable);

}
