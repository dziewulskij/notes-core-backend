package pl.dziewulskij.notesbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class NotesCoreBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotesCoreBackendApplication.class, args);
    }

}
