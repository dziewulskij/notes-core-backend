package pl.dziewulskij.notesbackend.webui.rest.user.validator

import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.validation.Errors
import pl.dziewulskij.notesbackend.core.jwt.JwtUser
import pl.dziewulskij.notesbackend.domain.user.model.User
import pl.dziewulskij.notesbackend.domain.user.repository.UserRepository
import pl.dziewulskij.notesbackend.webui.rest.user.dto.ChangeOwnPasswordDto
import spock.lang.Specification
import spock.lang.Unroll

class ChangeOwnPasswordValidatorTest extends Specification {

    private UserRepository userRepository = Mock()
    private PasswordEncoder passwordEncoder = Mock()
    private ChangeOwnPasswordValidator changeOwnPasswordValidator = new ChangeOwnPasswordValidator(
            userRepository,
            passwordEncoder
    )

    def 'supports should assignable from ChangeOwnPasswordDto'() {
        expect:
        changeOwnPasswordValidator.supports(ChangeOwnPasswordDto.class)
    }

    @Unroll('password: #password, confirmationPassword: #confirmationPassword, passwordEncoderMatches: #passwordEncoderMatches')
    def 'validate should correct valid for '() {
        given:
        def user = User.builder().password('oldPassword').build()
        def changeOwnPasswordDto = ChangeOwnPasswordDto.builder()
                .oldPassword('oldPassword')
                .password(password)
                .confirmationPassword(confirmationPassword)
                .build()
        def errors = Mock(Errors)
        def securityContext = Mock(SecurityContext)
        def authentication = Mock(Authentication)
        SecurityContextHolder.setContext(securityContext)
        securityContext.authentication >> authentication
        authentication.principal >> JwtUser.builder().id(1L).build()

        when:
        changeOwnPasswordValidator.validate(changeOwnPasswordDto, errors)

        then:
        1 * userRepository.findById(_ as Long) >> Optional.of(user)
        1 * passwordEncoder.matches(_ as String, _ as String) >> passwordEncoderMatches
        rejectInvocation * errors.rejectValue(_ as String, _ as String)

        where:
        password   | confirmationPassword   | passwordEncoderMatches || rejectInvocation
        'password' | 'confirmationPassword' | true                   || 1
        'password' | 'password'             | true                   || 0
        'password' | 'confirmationPassword' | false                  || 2
        'password' | 'password'             | false                  || 1
    }

}
