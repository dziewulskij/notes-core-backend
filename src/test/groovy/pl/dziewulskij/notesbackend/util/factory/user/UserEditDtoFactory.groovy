package pl.dziewulskij.notesbackend.util.factory.user


import pl.dziewulskij.notesbackend.webui.rest.user.dto.UserEditDto

class UserEditDtoFactory {

    static UserEditDto buildUserEditDto() {
        UserEditDto.builder()
                .id(1L)
                .name('editNameTest')
                .surname('editSurnameTest')
                .email('editEmailTest@dziewulskij.com')
                .enable(false)
                .authorityIds(Set.of(3L))
                .build()
    }

}
