package pl.dziewulskij.notesbackend.util.factory.user

import pl.dziewulskij.notesbackend.webui.rest.user.dto.UserSimpleDto

class UserSimpleDtoFactory {

    static UserSimpleDto buildUserSimpleDto() {
        UserSimpleDto.builder()
                .id(1L)
                .name('simpleNameTest')
                .surname('simpleSurnameTest')
                .email('simpleEmailTest')
                .build()
    }

}
