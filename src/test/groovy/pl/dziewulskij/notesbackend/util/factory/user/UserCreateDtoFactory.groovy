package pl.dziewulskij.notesbackend.util.factory.user


import pl.dziewulskij.notesbackend.webui.rest.user.dto.UserCreateDto

class UserCreateDtoFactory {

    static UserCreateDto buildUserCreateDto() {
        UserCreateDto.builder()
                .name('name')
                .surname('surname')
                .username('username')
                .email('email@ee.com')
                .password('password')
                .confirmationPassword('confirmationPassword')
                .build()
    }

}
