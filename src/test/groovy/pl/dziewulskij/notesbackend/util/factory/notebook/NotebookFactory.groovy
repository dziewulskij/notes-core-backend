package pl.dziewulskij.notesbackend.util.factory.notebook

import pl.dziewulskij.notesbackend.domain.notebook.model.Note
import pl.dziewulskij.notesbackend.domain.notebook.model.Notebook
import pl.dziewulskij.notesbackend.util.factory.user.UserFactory

import java.time.LocalDateTime

class NotebookFactory {

    static Notebook buildNotebook() {
        def note = Note.builder().id(1L).title('noteTitle').build()
        Notebook.builder()
                .id(1L)
                .title('notebookTitle')
                .owner(UserFactory.buildUser())
                .notes(Set.of(note))
                .createdAt(LocalDateTime.of(2020, 12, 12, 12, 12, 12))
                .updatedAt(LocalDateTime.of(2020, 12, 31, 12, 12, 12))
                .build()
    }

}
