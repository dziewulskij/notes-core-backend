package pl.dziewulskij.notesbackend.util.factory.user

import pl.dziewulskij.notesbackend.domain.user.model.User

import java.time.LocalDateTime

class UserFactory {

    static User buildUser() {
        User.builder()
                .id(1L)
                .name('name')
                .surname('surname')
                .username('username')
                .email('email@ee.com')
                .enable(true)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build()
    }
}
