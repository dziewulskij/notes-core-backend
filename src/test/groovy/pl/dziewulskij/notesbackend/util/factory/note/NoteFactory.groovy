package pl.dziewulskij.notesbackend.util.factory.note

import pl.dziewulskij.notesbackend.domain.notebook.model.Note
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.NoteCreateEditDto

import java.time.LocalDateTime

class NoteFactory {

    static buildNoteCreateEditDto() {
        NoteCreateEditDto.builder()
                .title('noteCreateTitle')
                .subtitle('noteCreateSubtitle')
                .content('noteCreateContent')
                .build()
    }

    static buildNote() {
        Note.builder()
                .id(1L)
                .title('noteCreateTitle')
                .subtitle('noteCreateSubtitle')
                .content('noteCreateContent')
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build()
    }

}
