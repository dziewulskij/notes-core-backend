package pl.dziewulskij.notesbackend.core.mapper.notebook

import org.mapstruct.factory.Mappers
import pl.dziewulskij.notesbackend.core.mapper.dictionary.DictionaryMapper
import pl.dziewulskij.notesbackend.domain.notebook.model.Notebook
import pl.dziewulskij.notesbackend.util.factory.notebook.NotebookFactory
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.BaseNotebookDto
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.NotebookDto
import spock.lang.Specification

class NotebookMapperTest extends Specification {

    private DictionaryMapper dictionaryMapper = Mappers.getMapper(DictionaryMapper.class)
    private NotebookMapper notebookMapper = new NotebookMapperImpl(dictionaryMapper)

    def 'mapToNotebook should map NotebookCreateDto to Notebook'() {
        given:
        def notebookCreateDto = BaseNotebookDto.builder()
                .title('title')
                .build()

        when:
        Notebook result = notebookMapper.mapToNotebook(notebookCreateDto)

        then:
        Objects.nonNull(result)
        Objects.isNull(result.id)
        result.title == notebookCreateDto.title
    }

    def 'mapToNotebookDto should map Notebook to NotebookDto'() {
        given:
        def notebook = NotebookFactory.buildNotebook()

        when:
        NotebookDto result = notebookMapper.mapToNotebookDto(notebook)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == notebook.id
            result.title == notebook.title
            result.createdAt == notebook.createdAt
            result.updatedAt == notebook.updatedAt
            result.notes.size() == notebook.notes.size()
            result.notes[0].id == notebook.notes[0].id
            result.notes[0].name == notebook.notes[0].title
        }
    }

    def 'mapToNotebook should map BaseNotebookDto with mapping target Notebook to Notebook'() {
        given:
        def notebook = NotebookFactory.buildNotebook()
        def baseNotebookDto = BaseNotebookDto.builder().title('editTitle').build()

        when:
        Notebook result = notebookMapper.mapToNotebook(baseNotebookDto, notebook)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == notebook.id
            result.title == baseNotebookDto.title
        }
    }

}
