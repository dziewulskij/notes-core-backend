package pl.dziewulskij.notesbackend.core.mapper.user

import pl.dziewulskij.notesbackend.core.mapper.dictionary.DictionaryMapper
import pl.dziewulskij.notesbackend.core.mapper.password.PasswordMapper
import pl.dziewulskij.notesbackend.core.mapper.user.UserMapper
import pl.dziewulskij.notesbackend.core.mapper.user.UserMapperImpl
import pl.dziewulskij.notesbackend.domain.user.model.User
import pl.dziewulskij.notesbackend.util.factory.user.UserEditDtoFactory
import pl.dziewulskij.notesbackend.util.factory.user.UserSimpleDtoFactory
import pl.dziewulskij.notesbackend.webui.rest.user.dto.UserDto
import spock.lang.Specification

import static pl.dziewulskij.notesbackend.util.factory.user.UserCreateDtoFactory.buildUserCreateDto
import static pl.dziewulskij.notesbackend.util.factory.user.UserFactory.buildUser

class UserMapperTest extends Specification {

    private PasswordMapper passwordMapper = Mock()
    private DictionaryMapper dictionaryMapper = Mock()
    private UserMapper userMapper = new UserMapperImpl(passwordMapper, dictionaryMapper)

    def 'mapToUserDto should map User to UserDto'() {
        given:
        def user = buildUser()

        when:
        UserDto result = userMapper.mapToUserDto(user)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == user.id
            result.name == user.name
            result.surname == user.surname
            result.username == user.username
            result.email == user.email
            result.enable == user.enable
            result.createdAt == user.createdAt
            result.updatedAt == user.updatedAt
        }
    }

    def 'mapToUser should map UserCreateDto to User'() {
        given:
        def encodePassword = 'encodePassword'
        def userCreateDto = buildUserCreateDto()
        1 * passwordMapper.map(_ as String) >> encodePassword

        when:
        User result = userMapper.mapToUser(userCreateDto)

        then:
        verifyAll {
            Objects.nonNull(result)
            Objects.isNull(result.id)
            result.name == userCreateDto.name
            result.surname == userCreateDto.surname
            result.username == userCreateDto.username
            result.email == userCreateDto.email
            result.password == encodePassword
        }
    }

    def 'mapToEdit should map UserEditDto with target User to User'() {
        given:
        def user = buildUser()
        def userEditDto = UserEditDtoFactory.buildUserEditDto()

        when:
        User result = userMapper.mapToEdit(userEditDto, user)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == user.id
            result.name == userEditDto.name
            result.surname == userEditDto.surname
            result.email == userEditDto.email
            result.enable == userEditDto.enable
            result.authorities.size() == userEditDto.authorityIds.size()
            result.authorities[0].id == userEditDto.authorityIds[0]
            result.password == user.password
        }
    }

    def 'mapToEdit should map UserSimpleDto with target User to User'() {
        given:
        def user = buildUser()
        def userSimpleDto = UserSimpleDtoFactory.buildUserSimpleDto()

        when:
        User result = userMapper.mapToEdit(userSimpleDto, user)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == user.id
            result.name == userSimpleDto.name
            result.surname == userSimpleDto.surname
            result.email == userSimpleDto.email
            result.enable == user.enable
            result.authorities.size() == user.authorities.size()
            result.password == user.password
        }
    }

}
