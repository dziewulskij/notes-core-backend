package pl.dziewulskij.notesbackend.core.mapper.password

import org.springframework.security.crypto.password.PasswordEncoder
import pl.dziewulskij.notesbackend.core.mapper.password.PasswordMapper
import spock.lang.Specification

class PasswordMapperTest extends Specification {

    private PasswordEncoder passwordEncoder = Mock()
    private PasswordMapper passwordMapper = new PasswordMapper(passwordEncoder)

    def 'should encode password text'() {
        given:
        def encodePassword = 'encodePassword'
        1 * passwordEncoder.encode(_ as String) >> encodePassword

        when:
        def result = passwordMapper.map('password')

        then:
        Objects.nonNull(result)
        result == encodePassword
    }

}
