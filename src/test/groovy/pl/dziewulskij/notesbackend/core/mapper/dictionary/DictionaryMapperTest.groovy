package pl.dziewulskij.notesbackend.core.mapper.dictionary

import org.mapstruct.factory.Mappers
import pl.dziewulskij.notesbackend.core.common.enumerated.AuthorityType
import pl.dziewulskij.notesbackend.domain.authority.model.Authority
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryDto
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryView
import pl.dziewulskij.notesbackend.domain.notebook.model.Note
import pl.dziewulskij.notesbackend.util.factory.user.UserFactory
import spock.lang.Specification

class DictionaryMapperTest extends Specification {

    private DictionaryMapper dictionaryMapper = Mappers.getMapper(DictionaryMapper.class)

    def 'mapToDictionaryDto should map DictionaryView to DictionaryDto'() {
        given:
        def dictionaryView = Mock(DictionaryView)
        dictionaryView.id >> 1L
        dictionaryView.name >> 'name'

        when:
        DictionaryDto result = dictionaryMapper.mapToDictionaryDto(dictionaryView)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == dictionaryView.id
            result.name == dictionaryView.name
        }
    }

    def 'mapToDictionaryDto should map Authority to DictionaryDto'() {
        given:
        def authority = new Authority(1L, AuthorityType.ROLE_ADMIN)

        when:
        DictionaryDto result = dictionaryMapper.mapToDictionaryDto(authority)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == authority.id
            result.name == authority.authorityType.name
        }
    }

    def 'mapToDictionaryDtos should map set of Authorities to set of DictionaryDtos'() {
        given:
        def authority = new Authority(1L, AuthorityType.ROLE_ADMIN)

        when:
        Set<DictionaryDto> result = dictionaryMapper.mapToDictionaryDtos(Set.of(authority))

        then:
        verifyAll {
            Objects.nonNull(result)
            result.size() == 1
            result[0] instanceof DictionaryDto
            result[0].id == authority.id
            result[0].name == authority.authorityType.name
        }
    }

    def 'mapToDictionaryDto should map User to DictionaryDto'() {
        given:
        def user = UserFactory.buildUser()

        when:
        DictionaryDto result = dictionaryMapper.mapToDictionaryDto(user)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == user.id
            result.name == user.name + ' ' + user.surname
        }
    }

    def 'mapToDictionaryDto should map Note to DictionaryDto'() {
        given:
        def note = Note.builder().id(1L).title('title').build()

        when:
        DictionaryDto result = dictionaryMapper.mapToDictionaryDto(note)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == note.id
            result.name == note.title
        }
    }

    def 'mapToDictionaryDto should map set of Note to list of DictionaryDto'() {
        given:
        def note = Note.builder().id(1L).title('title').build()

        when:
        List<DictionaryDto> result = dictionaryMapper.mapToDictionaryDto(Set.of(note))

        then:
        verifyAll {
            Objects.nonNull(result)
            result.size() == 1
            result[0].id == note.id
            result[0].name == note.title
        }
    }

}
