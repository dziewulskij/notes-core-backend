package pl.dziewulskij.notesbackend.core.mapper.note

import org.mapstruct.factory.Mappers
import pl.dziewulskij.notesbackend.domain.notebook.model.Note
import pl.dziewulskij.notesbackend.util.factory.note.NoteFactory
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.BaseNoteDto
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.NoteDto
import spock.lang.Specification

class NoteMapperTest extends Specification {

    private NoteMapper noteMapper = Mappers.getMapper(NoteMapper.class)

    def 'mapToNote should map NoteCreateDto to Note'() {
        given:
        def noteCreateEditDto = NoteFactory.buildNoteCreateEditDto()

        when:
        Note result = noteMapper.mapToNote(noteCreateEditDto)

        then:
        verifyAll {
            Objects.nonNull(result)
            Objects.isNull(result.id)
            result.title == noteCreateEditDto.title
            result.subtitle == noteCreateEditDto.subtitle
            result.content == noteCreateEditDto.content
        }
    }

    def 'mapToNoteDto should map Note to NoteDto'() {
        given:
        def note = NoteFactory.buildNote()

        when:
        NoteDto result = noteMapper.mapToNoteDto(note)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == note.id
            result.title == note.title
            result.subtitle == note.subtitle
            result.content == note.content
            result.createdAt == note.createdAt
            result.updatedAt == note.updatedAt
        }
    }

    def 'mapToBaseNoteDto should map Note to BaseNoteDto'() {
        given:
        def note = NoteFactory.buildNote()

        when:
        BaseNoteDto result = noteMapper.mapToBaseNoteDto(note)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == note.id
            result.title == note.title
            result.subtitle == note.subtitle
            result.createdAt == note.createdAt
        }
    }


    def 'mapToNote should map NoteCreateEditDto with mapping target Note to Note'() {
        given:
        def note = NoteFactory.buildNote()
        def noteCreateEditDto = NoteFactory.buildNoteCreateEditDto()

        when:
        Note result = noteMapper.mapToNote(noteCreateEditDto, note)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == note.id
            result.title == noteCreateEditDto.title
            result.subtitle == noteCreateEditDto.subtitle
            result.content == noteCreateEditDto.content
        }
    }

}
