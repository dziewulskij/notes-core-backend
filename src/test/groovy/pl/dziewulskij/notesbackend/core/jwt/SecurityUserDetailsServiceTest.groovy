package pl.dziewulskij.notesbackend.core.jwt

import org.springframework.security.core.userdetails.UsernameNotFoundException
import pl.dziewulskij.notesbackend.domain.user.service.UserDaoService
import spock.lang.Specification

class SecurityUserDetailsServiceTest extends Specification {

    private UserDaoService userDaoService = Mock()
    private SecurityUserDetailsService securityUserDetailsService = new SecurityUserDetailsService(userDaoService)

    def 'loadUserByUsername should load user by username and retrieve UserDetail'() {
        given:
        1 * userDaoService.findUserPrincipalByUsername(_ as String) >> Optional.of(Mock(UserPrincipal))

        when:
        def result = securityUserDetailsService.loadUserByUsername('usernameTest')

        then:
        Objects.nonNull(result)
        result instanceof UserPrincipal
    }

    def 'loadUserByUsername should thrown exception when user not exists'() {
        given:
        1 * userDaoService.findUserPrincipalByUsername(_ as String) >> Optional.empty()

        when:
        securityUserDetailsService.loadUserByUsername('usernameTest')

        then:
        thrown(UsernameNotFoundException)
    }

}
