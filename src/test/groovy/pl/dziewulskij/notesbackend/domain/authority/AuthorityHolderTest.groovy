package pl.dziewulskij.notesbackend.domain.authority

import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import pl.dziewulskij.notesbackend.core.jwt.JwtUser
import spock.lang.Specification
import spock.lang.Unroll

class AuthorityHolderTest extends Specification {

    private AuthorityHolder authorityHolder = new AuthorityHolder()

    @Unroll('for authorities: #authorities and loggedUserId: #loggedUserId and requestUserId: #requestUserId. Expected result: #expectResult')
    def 'hasAuthorityToRetrieveUserById '() {
        given:
        def securityContext = Mock(SecurityContext)
        def authentication = Mock(Authentication)
        def jwtUser = JwtUser.builder()
                .id(loggedUserId)
                .authorities(authorities)
                .build()
        SecurityContextHolder.setContext(securityContext)
        securityContext.authentication >> authentication
        authentication.principal >> jwtUser

        when:
        def result = authorityHolder.hasAuthorityToUserObjectById(requestUserId)

        then:
        result == expectResult

        where:
        authorities                                      | loggedUserId | requestUserId || expectResult
        Set.of(new SimpleGrantedAuthority("ROLE_USER"))  | 1L           | 1L            || true
        Set.of(new SimpleGrantedAuthority("ROLE_ADMIN")) | 1L           | 2L            || true
        Set.of(new SimpleGrantedAuthority("ROLE_ADMIN")) | 1L           | 1L            || true
        Set.of(new SimpleGrantedAuthority("ROLE_ADMIN"),
                new SimpleGrantedAuthority("ROLE_USER")) | 1L           | 2L            || true
        Set.of(new SimpleGrantedAuthority("ROLE_ADMIN"),
                new SimpleGrantedAuthority("ROLE_USER")) | 1L           | 1L            || true
        Set.of(new SimpleGrantedAuthority("ROLE_USER"))  | 1L           | 2L            || false
        Set.of()                                         | 1L           | 1L            || false
        Set.of()                                         | 1L           | 2L            || false
    }

}
