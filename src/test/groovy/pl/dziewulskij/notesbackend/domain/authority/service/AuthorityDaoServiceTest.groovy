package pl.dziewulskij.notesbackend.domain.authority.service

import pl.dziewulskij.notesbackend.domain.authority.repository.AuthorityRepository
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryView
import spock.lang.Specification

class AuthorityDaoServiceTest extends Specification {

    private AuthorityRepository authorityRepository = Mock()
    private AuthorityDaoService authorityDaoService = new AuthorityDaoService(authorityRepository)

    def 'getAllAuthoritiesDictionary should retrieve set of DictionaryView'() {
        given:
        def dictionaryView = Mock(DictionaryView)
        1 * authorityRepository.getAllAuthoritiesDictionaryView() >> Set.of(dictionaryView)

        when:
        def result = authorityDaoService.getAllAuthoritiesDictionary()

        then:
        Objects.nonNull(result)
        result instanceof Set
        result.size() == 1
        result[0] instanceof DictionaryView
    }

}
