package pl.dziewulskij.notesbackend.domain.authority.service

import pl.dziewulskij.notesbackend.core.mapper.dictionary.DictionaryMapper
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryDto
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryView
import spock.lang.Specification

class AuthorityServiceTest extends Specification {

    private AuthorityDaoService authorityDaoService = Mock()
    private DictionaryMapper dictionaryMapper = Mock()
    private AuthorityService authorityService = new AuthorityService(authorityDaoService, dictionaryMapper)

    def 'getAllAuthoritiesDictionary should retrieve all authorities as DictionaryDto'() {
        given:
        def dictionaryView = Mock(DictionaryView)
        def dictionaryDto = DictionaryDto.builder().build()
        1 * authorityDaoService.getAllAuthoritiesDictionary() >> Set.of(dictionaryView)
        1 * dictionaryMapper.mapToDictionaryDto(_ as DictionaryView) >> dictionaryDto

        when:
        def result = authorityService.getAllAuthoritiesDictionary()

        then:
        Objects.nonNull(result)
        result instanceof Set
        result.size() == 1
        result[0] instanceof DictionaryDto
    }

}
