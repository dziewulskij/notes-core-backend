package pl.dziewulskij.notesbackend.domain.notebook.service

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import pl.dziewulskij.notesbackend.core.exception.ObjectNotFoundException
import pl.dziewulskij.notesbackend.core.jwt.JwtUser
import pl.dziewulskij.notesbackend.core.mapper.dictionary.DictionaryMapper
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryDto
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryView
import pl.dziewulskij.notesbackend.domain.notebook.model.Notebook
import pl.dziewulskij.notesbackend.domain.notebook.repository.NotebookRepository
import pl.dziewulskij.notesbackend.domain.user.model.User
import pl.dziewulskij.notesbackend.domain.user.repository.UserRepository
import spock.lang.Specification

class NotebookDaoServiceTest extends Specification {

    private NotebookRepository notebookRepository = Mock()
    private UserRepository userRepository = Mock()
    private DictionaryMapper dictionaryMapper = Mock()
    private NotebookDaoService notebookDaoService = new NotebookDaoService(
            notebookRepository,
            userRepository,
            dictionaryMapper)

    def 'create should create new notebook'() {
        given:
        def notebook = Mock(Notebook)
        def user = Mock(User)
        prepareSecurityContextHolder()
        user.notebooks = new HashSet<>()
        1 * userRepository.findById(_ as Long) >> Optional.of(user)
        1 * notebookRepository.save(_ as Notebook) >> Mock(Notebook)

        when:
        def result = notebookDaoService.create(notebook)

        then:
        Objects.nonNull(result)
        result instanceof Notebook
    }

    def 'create should throw ObjectNotFoundException'() {
        given:
        def notebook = Mock(Notebook)
        prepareSecurityContextHolder()
        1 * userRepository.findById(_ as Long) >> Optional.empty()

        when:
        notebookDaoService.create(notebook)

        then:
        thrown(ObjectNotFoundException)
    }

    def "findNotebookById should retrieve Optional of Notebook"() {
        given:
        1 * notebookRepository.findById(_ as Long) >> Optional.of(Mock(Notebook))

        when:
        def result = notebookDaoService.findNotebookById(1L)

        then:
        Objects.nonNull(result)
        result instanceof Optional<Notebook>
    }

    def 'findNotebookDictionariesByUserId should retrieve dictionaries'() {
        given:
        1 * notebookRepository.findNotebookDictionariesByUserId(_ as Long) >> List.of(Mock(DictionaryView))
        1 * dictionaryMapper.mapToDictionaryDto(_ as DictionaryView)

        when:
        def result = notebookDaoService.findNotebookDictionariesByUserId(1L)

        then:
        Objects.nonNull(result)
        result instanceof List<DictionaryDto>
    }

    def 'getAllPageable should retrieve users pageable and filtering'() {
        given:
        def page = new PageImpl<>(List.of(Mock(Notebook)))
        1 * notebookRepository.findAllPageable(_ as Long, _ as Pageable) >> page

        when:
        def result = notebookDaoService.findAllNotebookPageable(1L, Mock(Pageable))

        then:
        Objects.nonNull(result)
        result instanceof Page<Notebook>
    }

    def "save should save Notebook"() {
        given:
        1 * notebookRepository.save(_ as Notebook) >> Mock(Notebook)

        when:
        def result = notebookDaoService.save(Mock(Notebook))

        then:
        Objects.nonNull(result)
        result instanceof Notebook
    }

    def 'deleteNotebookById should delete notebook'() {
        when:
        notebookDaoService.deleteById(1L)

        then:
        1 * notebookRepository.deleteById(_ as Long)
    }

    private void prepareSecurityContextHolder() {
        def context = Mock(SecurityContext)
        def authentication = Mock(Authentication)
        authentication.principal >> JwtUser.builder().id(1L).build()
        context.authentication >> authentication
        SecurityContextHolder.setContext(context)
    }

}
