package pl.dziewulskij.notesbackend.domain.notebook.service

import pl.dziewulskij.notesbackend.core.exception.ObjectNotFoundException
import pl.dziewulskij.notesbackend.domain.notebook.model.Note
import pl.dziewulskij.notesbackend.domain.notebook.model.Notebook
import pl.dziewulskij.notesbackend.domain.notebook.repository.NoteRepository
import pl.dziewulskij.notesbackend.domain.notebook.repository.NotebookRepository
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.NoteDto
import spock.lang.Specification

class NoteDaoServiceTest extends Specification {

    private NotebookRepository notebookRepository = Mock()
    private NoteRepository noteRepository = Mock()
    private NoteDaoService noteDaoService = new NoteDaoService(notebookRepository, noteRepository)

    def 'create should create new note'() {
        given:
        1 * notebookRepository.findById(_ as Long) >> Optional.of(Mock(Notebook))
        1 * noteRepository.save(_ as Note) >> Mock(Note)

        when:
        def result = noteDaoService.create(1L, Mock(Note))

        then:
        Objects.nonNull(result)
        result instanceof Note
    }

    def 'findAllByNotebookId should retrieve list of notes assigned to notebook'() {
        given:
        1 * noteRepository.findAllByNotebookId(_ as Long) >> List.of(Mock(Note))

        when:
        def result = noteDaoService.findAllNoteByNotebookId(1L)

        then:
        Objects.nonNull(result)
        result instanceof List<NoteDto>
    }

    def 'findById should retrieve Optional of Note'() {
        given:
        1 * noteRepository.findById(_ as Long) >> Optional.of(Mock(Note))

        when:
        def result = noteDaoService.findById(1L)

        then:
        Objects.nonNull(result)
        result instanceof Optional<Note>
    }

    def 'create should thrown thrownObjectNotFoundException'() {
        given:
        1 * notebookRepository.findById(_ as Long) >> Optional.empty()

        when:
        noteDaoService.create(1L, Mock(Note))

        then:
        thrown(ObjectNotFoundException)
    }

    def 'save should retrieve saved or updated Note'() {
        given:
        1 * noteRepository.save(_ as Note) >> Mock(Note)

        when:
        def result = noteRepository.save(Mock(Note))

        then:
        Objects.nonNull(result)
        result instanceof Note
    }

    def 'deleteById should delete note'() {
        when:
        noteDaoService.deleteById(1L)

        then:
        1 * noteRepository.deleteById(_ as Long)
    }

}
