package pl.dziewulskij.notesbackend.domain.notebook.service

import pl.dziewulskij.notesbackend.core.exception.ObjectNotFoundException
import pl.dziewulskij.notesbackend.core.mapper.note.NoteMapper
import pl.dziewulskij.notesbackend.domain.notebook.model.Note
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.NoteCreateEditDto
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.NoteDto
import spock.lang.Specification

class NoteServiceTest extends Specification {

    private NoteDaoService noteDaoService = Mock()
    private NoteMapper noteMapper = Mock()
    private NoteService noteService = new NoteService(noteDaoService, noteMapper)

    def 'createNote should create new note'() {
        given:
        1 * noteMapper.mapToNote(_ as NoteCreateEditDto) >> Mock(Note)
        1 * noteDaoService.create(_ as Long, _ as Note) >> Mock(Note)
        1 * noteMapper.mapToNoteDto(_ as Note) >> Mock(NoteDto)

        when:
        def result = noteService.createNote(1L, Mock(NoteCreateEditDto))

        then:
        Objects.nonNull(result)
        result instanceof NoteDto
    }

    def 'getAllNoteByNotebookId should create list of notes assigned to notebook'() {
        given:
        1 * noteDaoService.findAllNoteByNotebookId(_ as Long) >> List.of(Mock(Note))
        1 * noteMapper.mapToBaseNoteDto(_ as Note) >> Mock(NoteDto)

        when:
        def result = noteService.getAllNoteByNotebookId(1L)

        then:
        Objects.nonNull(result)
        result instanceof List<NoteDto>
    }

    def 'getNoteById should retrieve note'() {
        given:
        1 * noteDaoService.findById(_ as Long) >> Optional.of(Mock(Note))
        1 * noteMapper.mapToNoteDto(_ as Note) >> Mock(NoteDto)

        when:
        def result = noteService.getNoteById(1L)

        then:
        Objects.nonNull(result)
        result instanceof NoteDto
    }

    def 'getNoteById should thrown ObjectNotFoundException'() {
        given:
        1 * noteDaoService.findById(_ as Long) >> Optional.empty()

        when:
        noteService.getNoteById(1L)

        then:
        thrown(ObjectNotFoundException)
    }

    def 'editNoteById should update note'() {
        given:
        1 * noteDaoService.findById(_ as Long) >> Optional.of(Mock(Note))
        1 * noteMapper.mapToNote(_ as NoteCreateEditDto, _ as Note) >> Mock(Note)
        1 * noteDaoService.save(_ as Note) >> Mock(Note)
        1 * noteMapper.mapToNoteDto(_ as Note) >> Mock(NoteDto)

        when:
        def result = noteService.editNoteById(1L, Mock(NoteCreateEditDto))

        then:
        Objects.nonNull(result)
        result instanceof NoteDto
    }

    def 'deleteById should delete note'() {
        when:
        noteService.deleteNoteById(1L)

        then:
        1 * noteDaoService.deleteById(_ as Long)
    }

}
