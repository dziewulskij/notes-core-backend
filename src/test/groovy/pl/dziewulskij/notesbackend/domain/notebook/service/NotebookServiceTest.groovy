package pl.dziewulskij.notesbackend.domain.notebook.service

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import pl.dziewulskij.notesbackend.core.exception.ObjectNotFoundException
import pl.dziewulskij.notesbackend.core.jwt.JwtUser
import pl.dziewulskij.notesbackend.core.mapper.notebook.NotebookMapper
import pl.dziewulskij.notesbackend.domain.dictionary.DictionaryDto
import pl.dziewulskij.notesbackend.domain.notebook.model.Notebook
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.BaseNotebookDto
import pl.dziewulskij.notesbackend.webui.rest.notebook.dto.NotebookDto
import spock.lang.Specification

class NotebookServiceTest extends Specification {

    private NotebookDaoService notebookDaoService = Mock()
    private NotebookMapper notebookMapper = Mock()
    private NotebookService notebookService = new NotebookService(notebookDaoService, notebookMapper)

    def 'createNotebook should create new notebook'() {
        given:
        def notebookCreateDto = Mock(BaseNotebookDto)
        1 * notebookMapper.mapToNotebook(_ as BaseNotebookDto) >> Mock(Notebook)
        1 * notebookDaoService.create(_ as Notebook) >> Mock(Notebook)
        1 * notebookMapper.mapToNotebookDto(_ as Notebook) >> Mock(NotebookDto)

        when:
        def result = notebookService.createNotebook(notebookCreateDto)

        then:
        Objects.nonNull(result)
        result instanceof NotebookDto
    }

    def 'getNotebookById should retrieve notebook'() {
        given:
        1 * notebookDaoService.findNotebookById(_ as Long) >> Optional.of(Mock(Notebook))
        1 * notebookMapper.mapToNotebookDto(_ as Notebook) >> Mock(NotebookDto)

        when:
        def result = notebookService.getNotebookById(1L)

        then:
        Objects.nonNull(result)
        result instanceof NotebookDto
    }

    def 'getNotebookById should thrown ObjectNotFoundException'() {
        given:
        1 * notebookDaoService.findNotebookById(_ as Long) >> Optional.empty()

        when:
        notebookService.getNotebookById(1L)

        then:
        thrown(ObjectNotFoundException)
    }

    def 'getNotebookDictionaries should retrieve dictionaries'() {
        given:
        def dictionaryDto = DictionaryDto.builder().build()
        prepareSecurityContextHolder()
        1 * notebookDaoService.findNotebookDictionariesByUserId(_ as Long) >> List.of(dictionaryDto)

        when:
        def result = notebookService.getNotebookDictionaries()

        then:
        Objects.nonNull(result)
        result instanceof List<DictionaryDto>
    }

    def 'getAllNotebookPageable should retrieve Page of NotebookDto'() {
        given:
        prepareSecurityContextHolder()
        def page = new PageImpl<>(List.of(Mock(Notebook)))
        1 * notebookDaoService.findAllNotebookPageable(_ as Long, _ as Pageable) >> page
        1 * notebookMapper.mapToNotebookDto(_ as Notebook) >> Mock(NotebookDto)

        when:
        def result = notebookService.getAllNotebookPageable(Mock(Pageable))

        then:
        Objects.nonNull(result)
        result instanceof Page<NotebookDto>
    }

    def 'editNotebookById should edit Notebook'() {
        given:
        1 * notebookDaoService.findNotebookById(_ as Long) >> Optional.of(Mock(Notebook))
        1 * notebookMapper.mapToNotebook(_ as BaseNotebookDto, _ as Notebook) >> Mock(Notebook)
        1 * notebookDaoService.save(_ as Notebook) >> Mock(Notebook)
        1 * notebookMapper.mapToNotebookDto(_ as Notebook) >> Mock(NotebookDto)

        when:
        def result = notebookService.editNotebookById(1L, Mock(BaseNotebookDto))

        then:
        Objects.nonNull(result)
        result instanceof NotebookDto
    }

    def 'editNotebookById should thrown ObjectNotFoundException'() {
        given:
        1 * notebookDaoService.findNotebookById(_ as Long) >> Optional.empty()

        when:
        notebookService.editNotebookById(1L, Mock(BaseNotebookDto))

        then:
        thrown(ObjectNotFoundException)
    }

    def 'deleteNotebookById should delete notebook'() {
        when:
        notebookService.deleteNotebookById(1L)

        then:
        1 * notebookDaoService.deleteById(_ as Long)
    }

    private void prepareSecurityContextHolder() {
        def context = Mock(SecurityContext)
        def authentication = Mock(Authentication)
        authentication.principal >> JwtUser.builder().id(1L).build()
        context.authentication >> authentication
        SecurityContextHolder.setContext(context)
    }

}
