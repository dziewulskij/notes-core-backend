package pl.dziewulskij.notesbackend.domain.auth

import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import pl.dziewulskij.notesbackend.core.jwt.JwtTokenProvider
import pl.dziewulskij.notesbackend.webui.rest.auth.dto.UserLoginDto
import spock.lang.Specification

class AuthenticationServiceTest extends Specification {

    private AuthenticationManager authenticationManager = Mock()
    private JwtTokenProvider jwtTokenProvider = Mock()
    private AuthenticationService authenticationService = new AuthenticationService(authenticationManager, jwtTokenProvider)

    def 'login should login user and retrieve jwt token'() {
        given:
        def userLoginDto = new UserLoginDto('username', 'password')
        def authentication = Mock(Authentication)
        1 * authenticationManager.authenticate(_ as Authentication) >> authentication
        SecurityContextHolder.setContext(Mock(SecurityContext))
        1 * jwtTokenProvider.generateToken(_ as Authentication) >> 'token'

        when:
        def result = authenticationService.login(userLoginDto)

        then:
        Objects.nonNull(result)
        result == 'token'
    }

}
