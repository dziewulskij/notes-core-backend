package pl.dziewulskij.notesbackend.domain.user.service

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import pl.dziewulskij.notesbackend.core.common.enumerated.AuthorityType
import pl.dziewulskij.notesbackend.core.jwt.UserPrincipal
import pl.dziewulskij.notesbackend.domain.authority.model.Authority
import pl.dziewulskij.notesbackend.domain.authority.repository.AuthorityRepository
import pl.dziewulskij.notesbackend.domain.user.model.User
import pl.dziewulskij.notesbackend.domain.user.repository.UserRepository
import pl.dziewulskij.notesbackend.webui.rest.user.dto.UserDto
import pl.dziewulskij.notesbackend.webui.rest.user.param.UserFilterRequestParam
import spock.lang.Specification

import static pl.dziewulskij.notesbackend.core.common.enumerated.AuthorityType.ROLE_USER

class UserDaoServiceTest extends Specification {

    private UserRepository userRepository = Mock()
    private AuthorityRepository authorityRepository = Mock()
    private UserDaoService userDaoService = new UserDaoService(userRepository, authorityRepository)

    def 'findUserById should retrieve Optional of User'() {
        given:
        def user = Mock(User)
        1 * userRepository.findById(_ as Long) >> Optional.of(user)

        when:
        def result = userDaoService.findUserById(1L)

        then:
        Objects.nonNull(result)
        result.isPresent()
        result.get() == user
    }

    def 'findUserPrincipalByUsername should retrieve Optional of UserPrincipal'() {
        given:
        def user = Mock(User)
        def authority = Authority.builder().authorityType(ROLE_USER).build()
        user.authorities >> [authority]
        1 * userRepository.findByUsername(_ as String) >> Optional.of(user)

        when:
        def result = userDaoService.findUserPrincipalByUsername('test')

        then:
        Objects.nonNull(result)
        result.isPresent()
        result.get() instanceof UserPrincipal
    }

    def 'getAllPageable should retrieve users pageable and filtering'() {
        given:
        def userFilterRequestParam = Mock(UserFilterRequestParam)
        def pageable = Mock(Pageable)
        def page = new PageImpl<>(List.of(Mock(User)))
        1 * userRepository.findAllPageable(_ as UserFilterRequestParam, _ as Pageable) >> page

        when:
        def result = userDaoService.findAllPageable(userFilterRequestParam, pageable)

        then:
        Objects.nonNull(result)
        result instanceof Page<UserDto>
    }

    def 'createUser should retrieve saved User'() {
        given:
        def user = User.builder().build()
        1 * authorityRepository.findByAuthorityType(_ as AuthorityType) >> Optional.of(Mock(Authority))
        1 * userRepository.save(_ as User) >> Mock(User)

        when:
        def result = userDaoService.createUser(user)

        then:
        Objects.nonNull(result)
        result instanceof User
    }

    def 'editUser should retrieve edited User'() {
        given:
        1 * userRepository.save(_ as User) >> Mock(User)

        when:
        def result = userDaoService.editUser(Mock(User))

        then:
        Objects.nonNull(result)
        result instanceof User
    }

    def 'enableOrDisableUserById should deactivate user'() {
        given:
        def user = Mock(User)

        when:
        userDaoService.enableOrDisableUserById(1L, true)

        then:
        1 * userRepository.findById(_ as Long) >> Optional.of(user)
        1 * userRepository.save(_ as User) >> Mock(User)
    }

    def 'changeOwnPassword should change password'() {
        given:
        def user = User.builder().build()
        1 * userRepository.findById(_ as Long) >> Optional.of(user)

        when:
        userDaoService.changeOwnPassword(1L, 'newPassword')

        then:
        1 * userRepository.save(_ as User) >> Mock(User)
    }
}
