package pl.dziewulskij.notesbackend.domain.user.service

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import pl.dziewulskij.notesbackend.core.exception.ObjectNotFoundException
import pl.dziewulskij.notesbackend.core.jwt.JwtUser
import pl.dziewulskij.notesbackend.core.mapper.password.PasswordMapper
import pl.dziewulskij.notesbackend.core.mapper.user.UserMapper
import pl.dziewulskij.notesbackend.domain.user.model.User
import pl.dziewulskij.notesbackend.webui.rest.user.dto.ChangeOwnPasswordDto
import pl.dziewulskij.notesbackend.webui.rest.user.dto.UserCreateDto
import pl.dziewulskij.notesbackend.webui.rest.user.dto.UserDto
import pl.dziewulskij.notesbackend.webui.rest.user.dto.UserEditDto
import pl.dziewulskij.notesbackend.webui.rest.user.dto.UserSimpleDto
import pl.dziewulskij.notesbackend.webui.rest.user.param.UserFilterRequestParam
import spock.lang.Specification

class UserServiceTest extends Specification {

    private UserMapper userMapper = Mock()
    private PasswordMapper passwordMapper = Mock()
    private UserDaoService userDaoService = Mock()
    private UserService userService = new UserService(userMapper, passwordMapper, userDaoService)

    def 'createUser should create user from userCreateDto and retrieve UserDto'() {
        given:
        1 * userMapper.mapToUser(_ as UserCreateDto) >> Mock(User)
        1 * userDaoService.createUser(_ as User) >> Mock(User)
        1 * userMapper.mapToUserDto(_ as User) >> Mock(UserDto)

        when:
        def result = userService.createUser(Mock(UserCreateDto))

        then:
        Objects.nonNull(result)
        result instanceof UserDto
    }

    def 'getUserById should retrieve user by id'() {
        given:
        1 * userDaoService.findUserById(_ as Long) >> Optional.of(Mock(User))
        1 * userMapper.mapToUserDto(_ as User) >> Mock(UserDto)

        when:
        def result = userService.getUserById(1L)

        then:
        Objects.nonNull(result)
        result instanceof UserDto
    }

    def 'getUserById should thrown ObjectNotFoundException'() {
        given:
        1 * userDaoService.findUserById(_ as Long) >> Optional.empty()

        when:
        userService.getUserById(1L)

        then:
        thrown(ObjectNotFoundException)
    }

    def 'getAllPageable should retrieve users pageable and filtering'() {
        given:
        def userFilterRequestParam = Mock(UserFilterRequestParam)
        def pageable = Mock(Pageable)
        def page = new PageImpl<>(List.of(Mock(User)))
        1 * userDaoService.findAllPageable(_ as UserFilterRequestParam, _ as Pageable) >> page
        1 * userMapper.mapToUserDto(_ as User) >> Mock(UserDto)

        when:
        def result = userService.getAllPageable(userFilterRequestParam, pageable)

        then:
        Objects.nonNull(result)
        result instanceof Page<UserDto>
    }

    def 'editUserByAdmin should edit User from UserEditDto'() {
        given:
        def userEditDto = UserEditDto.builder().id(1L).build()
        1 * userDaoService.findUserById(_ as Long) >> Optional.of(Mock(User))
        1 * userMapper.mapToEdit(_ as UserEditDto, _ as User) >> Mock(User)
        1 * userDaoService.editUser(_ as User) >> Mock(User)
        1 * userMapper.mapToUserDto(_ as User) >> Mock(UserDto)

        when:
        def result = userService.editUserByAdmin(userEditDto)

        then:
        Objects.nonNull(result)
        result instanceof UserDto
    }

    def 'editUserByClient should edit User from UserSimpleDto'() {
        given:
        def userSimpleDto = UserSimpleDto.builder().id(1L).build()
        1 * userDaoService.findUserById(_ as Long) >> Optional.of(Mock(User))
        1 * userMapper.mapToEdit(_ as UserSimpleDto, _ as User) >> Mock(User)
        1 * userDaoService.editUser(_ as User) >> Mock(User)
        1 * userMapper.mapToUserDto(_ as User) >> Mock(UserDto)

        when:
        def result = userService.editUserByClient(userSimpleDto)

        then:
        Objects.nonNull(result)
        result instanceof UserDto
    }

    def 'editUserByAdmin should thrown ObjectNotFoundException'() {
        given:
        def userEditDto = UserEditDto.builder().id(1L).build()
        1 * userDaoService.findUserById(_ as Long) >> Optional.empty()

        when:
        userService.editUserByAdmin(userEditDto)

        then:
        thrown(ObjectNotFoundException)
    }

    def 'editUserByClient should thrown ObjectNotFoundException'() {
        given:
        def userSimpleDto = UserSimpleDto.builder().id(1L).build()
        1 * userDaoService.findUserById(_ as Long) >> Optional.empty()

        when:
        userService.editUserByClient(userSimpleDto)

        then:
        thrown(ObjectNotFoundException)
    }

    def 'enableOrDisableUserById should deactivation user'() {
        when:
        userService.enableOrDisableUserById(1L, true)

        then:
        1 * userDaoService.enableOrDisableUserById(_ as Long, _ as Boolean)
    }

    def 'changeOwnPassword should changepassword'() {
        given:
        def changeOwnPasswordDto = Mock(ChangeOwnPasswordDto)
        def securityContext =  Mock(SecurityContext)
        def authentication =  Mock(Authentication)
        SecurityContextHolder.setContext(securityContext)
        securityContext.authentication >> authentication
        authentication.principal >> JwtUser.builder().id(1L).build()
        changeOwnPasswordDto.password >> 'password'

        when:
        userService.changeOwnPassword(changeOwnPasswordDto)

        then:
        1 * passwordMapper.map(_ as String) >> 'encodedPassword'
        1 * userDaoService.changeOwnPassword(_ as Long, _ as String)
    }

}
